package com.brighteyes.mykora.ui.matchInfo;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brighteyes.mykora.R;
import com.brighteyes.mykora.entities.Match;
import com.brighteyes.mykora.entities.MediaSource;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.ui.base.BaseSupportFragment;
import com.brighteyes.mykora.ui.base.PagerAdapter;
import com.brighteyes.mykora.ui.custom.NonSwipeableViewPager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MatchLiveFragment extends BaseSupportFragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_MATCH = "match";
    @BindView(R.id.live_viewpager)
    NonSwipeableViewPager mainViewPager;
    @BindView(R.id.live_tabs)
    TabLayout tabLayout;
    //    @BindView(R.id.adView)
//    AdView mAdView;
    private OnFragmentInteractionListener mListener;
    private PagerAdapter mPagerAdapter;
    private Match match;

    private List<MediaSource> mediaSources = new ArrayList<>();

    public MatchLiveFragment() {
        // Required empty public constructor
    }

    public static MatchLiveFragment newInstance(String title, Match match) {
        MatchLiveFragment fragment = new MatchLiveFragment();
        fragment.setTitle(title);
        Bundle args = new Bundle();
        args.putParcelable(ARG_MATCH, Parcels.wrap(match));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            match = Parcels.unwrap(getArguments().getParcelable(ARG_MATCH));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_match_live, container, false);
        ButterKnife.bind(this, rootView);
        getChannelMedia();

//        loadAd(mAdView);
        addDividers(tabLayout);

        return rootView;
    }

    private void getChannelMedia() {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(CommonConstants.FB_NODE_CHANNEL_MEDIA)
                .child(String.valueOf(match.getChannelID()))
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        mediaSources.clear();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            mediaSources.add(new MediaSource().parseMediaSource(child));
                        }
                        Log.d(CommonConstants.TAG, "ch media size : " + mediaSources.size());
                        initViewPager();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void initViewPager() {
        int defaultTab = 0;

        List<BaseSupportFragment> fragments = new ArrayList<>();
        List<String> tabTitles = new ArrayList<>();

        for (int i = 0; i < mediaSources.size(); i++) {
            MediaSource mediaSource = mediaSources.get(i);
            if (mediaSource.getType() == 1) {
                fragments.add(IFrameFragment.newInstance(mediaSource));
            } else {
                mediaSource.setThumbnail(match.getChampLogoURL());
                fragments.add(VideoViewFragment.newInstance(mediaSource));
            }
            tabTitles.add(mediaSource.getName());

            if (mediaSource.isDefault()) {
                defaultTab = i;
            }
        }

        mPagerAdapter = new PagerAdapter(getChildFragmentManager(), fragments);
        mainViewPager.setAdapter(mPagerAdapter);
        tabLayout.setupWithViewPager(mainViewPager);

        TabLayout.Tab tab = tabLayout.getTabAt(defaultTab);
        if (tab != null) {
            tab.select();
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
