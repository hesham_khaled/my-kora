package com.brighteyes.mykora.ui.settings;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.brighteyes.mykora.App;
import com.brighteyes.mykora.R;
import com.brighteyes.mykora.entities.Country;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shanan on 22/03/2017.
 */

public class TimeAdapter extends RecyclerView.Adapter<TimeAdapter.ViewHolder> {
    private Activity mContext;
    private List<Country> countriesList;
    private Typeface typeface;
    private SettingsHelper mSettingsHelper;
    private int lastCheckedPosition = -1;

    public TimeAdapter(Activity context, SettingsHelper settingsHelper,
                       List<Country> videosList) {
        mContext = context;
        this.mSettingsHelper = settingsHelper;
        this.countriesList = videosList;
        typeface = App.defaultFont;
    }

    @Override
    public TimeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(mContext).inflate(R.layout.timezone_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final Country timeZoneItem = countriesList.get(position);

        if (timeZoneItem != null) {
            holder.country.setText(timeZoneItem.getName());
            if (timeZoneItem.getTimeZone() > 0) {
                holder.timeRadio.setText("GMT +" + timeZoneItem.getTimeZone());
            } else {
                holder.timeRadio.setText("GMT " + timeZoneItem.getTimeZone());
            }
            // default selection
            if (lastCheckedPosition == -1) {
                // check the settings' value
                holder.timeRadio.setChecked(position == mSettingsHelper.getTimezoneIndex());
            } else {
                holder.timeRadio.setChecked(position == lastCheckedPosition);
            }
        }
    }

    @Override
    public int getItemCount() {
        return countriesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.countryContainer)
        ViewGroup countryContainer;
        @BindView(R.id.time_radio)
        RadioButton timeRadio;
        @BindView(R.id.country)
        TextView country;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            country.setTypeface(typeface);
            timeRadio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    lastCheckedPosition = getAdapterPosition();
                    notifyItemRangeChanged(0, countriesList.size());
                    // save to shared references and settings helper
                    mSettingsHelper.changeTimezone(countriesList.get(lastCheckedPosition).getTimeZone(),
                            lastCheckedPosition, mContext);
                }
            });
        }
    }

}
