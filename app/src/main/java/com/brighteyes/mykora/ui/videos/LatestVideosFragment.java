package com.brighteyes.mykora.ui.videos;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.brighteyes.mykora.R;
import com.brighteyes.mykora.entities.Video;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.helper.Utils;
import com.brighteyes.mykora.ui.base.BaseSupportFragment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LatestVideosFragment extends BaseSupportFragment {

    private static final int PER_PAGE = 10;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.retry)
    TextView retry;
    @BindView(R.id.tvMessage)
    TextView tvMessage;
    @BindView(R.id.tryAgainLayout)
    ViewGroup tryAgainLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.loadMoreButton)
    Button loadMoreButton;

    private VideosAdapter mVideosAdapter;
    private List<Video> mVideos = new ArrayList<>();
    private HashMap<String, Video> mVideosHash = new HashMap<>();

    public LatestVideosFragment() {
        // Required empty public constructor
    }

    public static LatestVideosFragment newInstance() {
        LatestVideosFragment fragment = new LatestVideosFragment();
        fragment.setTitle("أحدث الفيديوهات");
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_recycler, container, false);
        ButterKnife.bind(this, rootView);
        getLatestVideos();
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadMoreButton.setTypeface(mTypeface);
        loadMoreButton.setVisibility(View.VISIBLE);
        loadMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCategoryVideosWithPagination();
            }
        });

    }

    private void getCategoryVideosWithPagination() {

        try {
            if (!Utils.getNetworkState(getActivity())) {
                showTryAgainLayout(R.string.connection_error);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        showProgressBar(true);
//        hideTryAgainLayout();

        FirebaseDatabase.getInstance()
                .getReference()
                .child(CommonConstants.FB_NODE_VIDEO)
                .orderByChild(CommonConstants.FB_CAT_VID_CREATION_DATE)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (mVideos.size() > 0) {
                            dataSnapshot.getRef().orderByKey().startAt(mVideos.get(mVideos.size() - 1).getId())
                                    .limitToLast(PER_PAGE).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                                        Video v = new Video().parseVideo(child);
                                        mVideos.add(v);
                                        mVideosHash.put(v.getId(), v);
                                    }
                                    showProgressBar(false);
                                    if (mVideos.size() == 0) {
                                        showTryAgainLayout(R.string.no_videos);
                                    }
                                    updateVideos();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        } else {
                            showProgressBar(false);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void getLatestVideos() {

        try {
            if (!Utils.getNetworkState(getActivity())) {
                showTryAgainLayout(R.string.connection_error);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        showProgressBar(true);
        hideTryAgainLayout();

        FirebaseDatabase.getInstance()
                .getReference()
                .child(CommonConstants.FB_NODE_VIDEO)
                .orderByChild(CommonConstants.FB_VID_CREATION_DATE)
                .limitToLast(PER_PAGE)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        mVideos.clear();
                        mVideosHash.clear();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            Video v = new Video().parseVideo(child);
                            mVideos.add(v);
                            mVideosHash.put(v.getId(), v);
                        }
                        showProgressBar(false);
                        if (mVideos.size() == 0) {
                            showTryAgainLayout(R.string.no_videos);
                            loadMoreButton.setVisibility(View.GONE);
                        }
                        updateVideos();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void updateVideos() {
        initVideosRV();
        if (mVideos.size() % PER_PAGE != 0) {
            loadMoreButton.setVisibility(View.GONE);
        }
    }

    private int getVideoPosition(String id) {
        int index = -1;
        for (Video video : mVideos) {
            if (!video.getId().equals("") && video.getId().equals(id)) {
                index = mVideos.indexOf(video);
                break;
            }
        }
        return index;
    }

    private void initVideosRV() {
        recyclerView.setHasFixedSize(true);
        mVideosAdapter = new VideosAdapter(getActivity(), mVideos, false, mNavigator);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mVideosAdapter);
    }

    @OnClick(R.id.retry)
    public void Onclick() {
        getLatestVideos();
    }

    private void showTryAgainLayout(int resId) {
        if (tryAgainLayout != null) {
            tryAgainLayout.setVisibility(View.VISIBLE);
            tvMessage.setTypeface(mTypeface);
            retry.setTypeface(mTypeface);
            tvMessage.setText(resId);
        }
    }

    private void hideTryAgainLayout() {
        if (tryAgainLayout != null) {
            tryAgainLayout.setVisibility(View.GONE);
        }
    }

    private void showProgressBar(boolean show) {
        if (progressBar != null) {
            if (show) {
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setIndeterminate(show);
            } else {
                progressBar.setVisibility(View.GONE);
            }
        }
    }
}