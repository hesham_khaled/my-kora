package com.brighteyes.mykora.entities;

import android.util.Log;

import com.brighteyes.mykora.helper.CommonConstants;
import com.google.firebase.database.DataSnapshot;

import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcel;

/**
 * Created by Shanan on 15/02/2017.
 */
@Parcel
public class Video {
    String id;
    String creationDate;
    String categoryName;
    String categoryID;
    String description;
    String name;
    String tags;
    String thumb_url;
    String videoUrl;
    String videoIFrame;
    boolean isImportant;
    int viewsCount;

    public Video() {
    }

    public Video parseVideo(DataSnapshot dataSnapshot) {
        this.id = dataSnapshot.getKey();
        Log.d("video",
                "creation date: " + dataSnapshot.child(CommonConstants.FB_VID_CREATION_DATE).getValue());
        //this.creationDate = Utils.getDateTime(time);
        this.description = (String) dataSnapshot.child(CommonConstants.FB_VID_DESCRIPTION).getValue();
        this.isImportant = (boolean) dataSnapshot.child(CommonConstants.FB_VID_IS_IMPORTANT).getValue();
        this.name = (String) dataSnapshot.child(CommonConstants.FB_VID_NAME).getValue();
        this.tags = (String) dataSnapshot.child(CommonConstants.FB_VID_TAGS).getValue();
        this.thumb_url = (String) dataSnapshot.child(CommonConstants.FB_VID_THUMB_URL).getValue();
        this.videoUrl = (String) dataSnapshot.child(CommonConstants.FB_VID_VIDEO_URL).getValue();
        this.videoIFrame = (String) dataSnapshot.child(CommonConstants.FB_VID_VIDEO_FRAME).getValue();
        this.categoryID = (String) dataSnapshot.child(CommonConstants.FB_VID_CAT_ID).getValue();
        this.categoryName = (String) dataSnapshot.child(CommonConstants.FB_VID_CAT_NAME).getValue();
        return this;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getId() {
        return id;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getTags() {
        return tags;
    }

    public String getThumb_url() {
        return thumb_url;
    }

    public void setThumb_url(String thumb_url) {
        this.thumb_url = thumb_url;
    }

    public boolean isImportant() {
        return isImportant;
    }

    public int getViewsCount() {
        return viewsCount;
    }


    public String getVideoIFrame() {
        return videoIFrame;
    }

    public void setVideoIFrame(String videoIFrame) {
        this.videoIFrame = videoIFrame;
    }

    public int getMediaType() {
        boolean notUrl = (videoUrl == null || videoUrl.isEmpty());
        boolean notIFrame = (videoIFrame == null || videoIFrame.isEmpty());
        if (notIFrame && notUrl) {
            return -1;
        } else if (notIFrame) {
            return CommonConstants.MEDIA_TYPE_URL;
        } else {
            return CommonConstants.MEDIA_TYPE_FRAME;
        }
    }

    public Video parseVideo(JSONObject videoObject) throws JSONException {
        Video video = new Video();
        video.id = videoObject.getString("$id");
        video.description = videoObject.getString(CommonConstants.FB_VID_DESCRIPTION);
        video.isImportant = videoObject.getBoolean(CommonConstants.FB_VID_IS_IMPORTANT);
        video.name = videoObject.getString(CommonConstants.FB_VID_NAME);
        video.tags = videoObject.getString(CommonConstants.FB_VID_TAGS);
        video.thumb_url = videoObject.getString(CommonConstants.FB_VID_THUMB_URL);
        video.videoUrl = videoObject.getString(CommonConstants.FB_VID_VIDEO_URL);
        video.categoryID = videoObject.getString(CommonConstants.FB_VID_CAT_ID);
        video.categoryName = videoObject.getString(CommonConstants.FB_VID_CAT_NAME);

        return video;
    }
}
