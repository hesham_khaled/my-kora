package com.brighteyes.mykora.ui.matches;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.brighteyes.mykora.App;
import com.brighteyes.mykora.R;
import com.brighteyes.mykora.Singleton;
import com.brighteyes.mykora.entities.League;
import com.brighteyes.mykora.entities.Match;
import com.brighteyes.mykora.nav.Navigator.Navigator;
import com.brighteyes.mykora.ui.settings.SettingsHelper;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shanan on 4/5/16.
 */
class SectionsAdapter extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder> {

    // Store a member variable for the sponsors sections
    private List<League> mLeagues;
    private Typeface mTypeface;
    private Navigator mNavigator;
    private Context mContext;
    private SettingsHelper mSettingsHelper;
    private InterstitialAd mInterstitialAd;

    // Pass in the contact array into the constructor
    SectionsAdapter(Context context, List<League> leagues, Navigator navigator,
                    SettingsHelper settingsHelper) {
        mContext = context;
        mTypeface = App.defaultFont;
        mLeagues = leagues;
        this.mNavigator = navigator;
        this.mSettingsHelper = settingsHelper;
        prepareInterstitialAd();
    }

    private void prepareInterstitialAd() {
        if (Singleton.getInstance().getAdsIds() != null && mContext != null) {
            mInterstitialAd = new InterstitialAd(mContext);
            mInterstitialAd.setAdUnitId(Singleton.getInstance().getAdsIds().getInterstitialAdId());
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
        }
    }

    @Override
    public int getItemViewType(int section, int relativePosition, int absolutePosition) {
        //if (section == 1) {
        //  return 0; // VIEW_TYPE_HEADER is -2, VIEW_TYPE_ITEM is -1. You can return 0 or greater.
        //}
        return super.getItemViewType(section, relativePosition, absolutePosition);

    }

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case VIEW_TYPE_HEADER:
                return new LeagueViewHolder(
                        inflater.inflate(R.layout.league_item_header, parent, false));
            case VIEW_TYPE_ITEM:
                return new MatchViewHolder(inflater.inflate(R.layout.match_item, parent, false));
        }

        return null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder viewHolder, int section) {
        LeagueViewHolder leagueViewHolder = (LeagueViewHolder) viewHolder;
        leagueViewHolder.leagueTitle.setText(mLeagues.get(section).getName());
    }


    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int section,
                                 int relativePosition, int absolutePosition) {
        MatchViewHolder matchViewHolder = (MatchViewHolder) viewHolder;
        final Match match = mLeagues.get(section).getMatches().get(relativePosition);

        if (match != null) {
            matchViewHolder.container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mNavigator.navigateToMatchInfoActivity(match);
                }
            });
            Picasso.with(mContext)
                    .load(match.getLeftLogo())
                    .fit()
                    .placeholder(R.drawable.logo_transparent)
                    .into(matchViewHolder.leftLogo);
            Picasso.with(mContext)
                    .load(match.getRightLogo())
                    .fit()
                    .placeholder(R.drawable.logo_transparent)
                    .into(matchViewHolder.rightLogo);
            matchViewHolder.leftName.setText(match.getLeftName());
            matchViewHolder.rightName.setText(match.getRightName());
            matchViewHolder.matchState.setText(match.getMatchStatusString());
            switch (match.getMatchStatus()) {
                case 0:
                    matchViewHolder.matchState.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
                    break;
                case 1:
                    matchViewHolder.matchState.setBackgroundColor(mContext.getResources().getColor(R.color.liveRed));
                    break;
                case 2:
                    matchViewHolder.matchState.setBackgroundColor(mContext.getResources().getColor(R.color.finishedDarkGray));
                    break;
            }
            matchViewHolder.matchStartTime.setText(match.getTime(mSettingsHelper));
            if (match.getMatchStatus() != 0) {
                matchViewHolder.leftGoals.setText(String.valueOf(match.getLeftScore()));
                matchViewHolder.rightGoals.setText(String.valueOf(match.getRightScore()));
            }
        }
    }

    @Override
    public int getSectionCount() {
        return mLeagues.size();
    }

    @Override
    public int getItemCount(int section) {
        return mLeagues.get(section).getMatches().size();
    }

    public void clear() {
        mLeagues.clear();
        notifyDataSetChanged();
    }

    public List<League> getLeagues() {
        return mLeagues;
    }

    //// Set a list of items
    public void setLeagues(List<League> leagues) {
        mLeagues.clear();
        mLeagues.addAll(leagues);
        notifyDataSetChanged();
    }

    // Add a list of items
    public void addLeagues(List<League> leagueList) {
        int positionStart = getItemCount();
        mLeagues.addAll(leagueList);
        notifyItemRangeInserted(positionStart, leagueList.size());
    }

    public void addLeague(League league) {
        int positionStart = getItemCount();
        mLeagues.add(league);
        notifyItemInserted(positionStart);
    }

    class LeagueViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.league_title)
        TextView leagueTitle;

        LeagueViewHolder(View itemView) {

            super(itemView);

            ButterKnife.bind(this, itemView);
            leagueTitle.setTypeface(mTypeface);
        }
    }

    class MatchViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.container)
        ViewGroup container;
        @BindView(R.id.right_logo)
        ImageView rightLogo;
        @BindView(R.id.left_logo)
        ImageView leftLogo;
        @BindView(R.id.right_name)
        TextView rightName;
        @BindView(R.id.left_name)
        TextView leftName;
        @BindView(R.id.right_goals)
        TextView rightGoals;
        @BindView(R.id.left_goals)
        TextView leftGoals;
        @BindView(R.id.match_state)
        TextView matchState;
        @BindView(R.id.match_start)
        TextView matchStartTime;

        MatchViewHolder(View itemView) {

            super(itemView);
            ButterKnife.bind(this, itemView);

            rightName.setTypeface(mTypeface);
            leftName.setTypeface(mTypeface);
            matchState.setTypeface(mTypeface);
        }
    }
}