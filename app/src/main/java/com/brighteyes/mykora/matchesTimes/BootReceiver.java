package com.brighteyes.mykora.matchesTimes;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.brighteyes.mykora.helper.CommonConstants;

/**
 * Created by Shanan on 16/08/2017.
 */

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(CommonConstants.TAG, "BootReceiver");
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            context.startService(new Intent(context, TimingService.class));
        }
    }
}
