package com.brighteyes.mykora.ui.base;

import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.brighteyes.mykora.App;
import com.brighteyes.mykora.R;
import com.brighteyes.mykora.Singleton;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.nav.Navigator.Navigator;
import com.brighteyes.mykora.pref.SharedPreferencesHelper;
import com.brighteyes.mykora.ui.settings.SettingsHelper;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by shanan on 13/07/2016.
 */
public class BaseSupportFragment extends Fragment {
    @Inject
    public Navigator mNavigator;
    @Inject
    public SharedPreferencesHelper mSharedPreferencesHelper;
    protected SettingsHelper mSettingsHelper;
    protected Typeface mTypeface;
    private String title;
    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getAppComponent().inject(this);
        //App.get(getActivity()).getAppComponent().inject(this);

        mNavigator.setContext(getActivity());
        mSettingsHelper = SettingsHelper.getInstance(getActivity(), mSharedPreferencesHelper);
        mTypeface = App.defaultFont;
    }

    protected View getLayout(@LayoutRes int layoutResID, LayoutInflater inflater,
                             ViewGroup container) {
        View rootView = inflater.inflate(layoutResID, container, false);

        unbinder = ButterKnife.bind(this, rootView);

        return rootView;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    protected void displayImagePicasso(final String s, final ImageView imageView) {
        Picasso.with(getActivity()).setLoggingEnabled(true);
        Picasso.with(getActivity())
                .load(s)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .placeholder(R.drawable.logo_transparent)
                .fit()
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {

                        //Try again online if cache failed
                        Picasso.with(getActivity())
                                .load(s)
                                .error(R.drawable.logo_transparent)
                                .placeholder(R.drawable.logo_transparent)
                                .into(imageView, new Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError() {
                                        Log.v("Picasso", "Could not fetch image");
                                    }
                                });
                    }
                });
    }

    protected void addDividers(TabLayout mainTabLayout) {

        LinearLayout linearLayout = (LinearLayout) mainTabLayout.getChildAt(0);
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(Color.GRAY);
        drawable.setSize(1, 1);
        linearLayout.setDividerPadding(20);
        linearLayout.setDividerDrawable(drawable);
    }

    protected void loadAd(AdView adView) {
        try {
            if (Singleton.getInstance().getAdsIds() != null) {
                adView.setAdUnitId(Singleton.getInstance().getAdsIds().getBannerAdId());
            }
            AdRequest adRequest = new AdRequest.Builder().build();
            adView.loadAd(adRequest);
        } catch (Exception ex) {
            Log.d(CommonConstants.TAG, "Couldn't load ad, " + ex.getLocalizedMessage());
        }
    }
}
