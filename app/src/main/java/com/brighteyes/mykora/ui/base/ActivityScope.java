package com.brighteyes.mykora.ui.base;

import javax.inject.Scope;

@Scope
public @interface ActivityScope {
}
