package com.brighteyes.mykora.ui.videos;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brighteyes.mykora.App;
import com.brighteyes.mykora.R;
import com.brighteyes.mykora.Singleton;
import com.brighteyes.mykora.entities.Video;
import com.brighteyes.mykora.helper.EndlessAdapter;
import com.brighteyes.mykora.nav.Navigator.Navigator;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shanan on 30/01/2017.
 */

public class VideosAdapter extends EndlessAdapter<Video> {

    private Activity mContext;
    private List<Video> mVideosList;
    private Typeface typeface;
    private boolean isRecommended;
    private Navigator navigator;
    private InterstitialAd mInterstitialAd;

    public VideosAdapter(Activity context, List<Video> mVideosList, boolean isRecommended,
                         Navigator navigator) {
        mContext = context;
        this.mVideosList = mVideosList;
        Collections.reverse(mVideosList);
        this.navigator = navigator;
        this.isRecommended = isRecommended;
        typeface = App.defaultFont;
        prepareInterstitialAd();
    }

    private void prepareInterstitialAd() {
        if (Singleton.getInstance().getAdsIds() != null && mContext != null) {
            mInterstitialAd = new InterstitialAd(mContext);
            mInterstitialAd.setAdUnitId(Singleton.getInstance().getAdsIds().getInterstitialAdId());
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
        }
    }


    @Override
    protected RecyclerView.ViewHolder createMyViewHolder(ViewGroup parent) {
        View view;
        if (isRecommended) {
            view = LayoutInflater.from(mContext).inflate(R.layout.recommended_video_item, parent, false);
        } else {
            view = LayoutInflater.from(mContext).inflate(R.layout.video_item, parent, false);
        }

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    protected void bindMyViewController(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;

        final Video videoItem = mVideosList.get(position);
        if (videoItem != null) {
            bindData(viewHolder, videoItem);
        }
    }

    private void bindData(final ViewHolder holder, final Video video) {
        Picasso.with(mContext)
                .load(video.getThumb_url())
                .placeholder(R.drawable.logo_transparent)
                .fit()
                .into(holder.thumb);
        holder.title.setText(video.getName());
        holder.thumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoVideoActivity(video);
            }
        });
    }

    private void gotoVideoActivity(Video video) {
        navigator.navigateToVideoDetailsActivity(video);
        if (mContext instanceof VideoDetailsActivity) {
            mContext.finish();
        }
    }

    @Override
    public int getItemCount() {
        return mVideosList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.thumb)
        ImageView thumb;
        @BindView(R.id.play)
        ImageView play;
        @BindView(R.id.video_title)
        TextView title;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            title.setTypeface(typeface);
            title.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    title.setSelected(true);
                    return false;
                }
            });
        }
    }
}