package com.brighteyes.mykora.entities;

import com.brighteyes.mykora.helper.CommonConstants;
import com.google.firebase.database.DataSnapshot;

import org.parceler.Parcel;

/**
 * Created by Shanan on 11/02/2017.
 */

@Parcel
public class Category {
    String id;
    String name;
    //public List<Video> videos = new ArrayList<>();

    public Category() {
    }

    public Category(String id, String name, String logo) {
        this.id = id;
        this.name = name;
    }

    public Category parseCategory(DataSnapshot dataSnapshot) {
        this.id = dataSnapshot.getKey();
        this.name = (String) dataSnapshot.child(CommonConstants.FB_VID_CATEGORY_NAME).getValue();
        return this;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    //public List<Video> getVideos() {
    //  return videos;
    //}
    //
    //public void setVideos(List<Video> videos) {
    //  this.videos = videos;
    //}
}
