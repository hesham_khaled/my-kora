package com.brighteyes.mykora.ui.matchInfo;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brighteyes.mykora.R;
import com.brighteyes.mykora.Singleton;
import com.brighteyes.mykora.entities.Match;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.ui.base.BaseActivity;
import com.brighteyes.mykora.ui.base.BaseSupportFragment;
import com.brighteyes.mykora.ui.base.PagerAdapter;
import com.brighteyes.mykora.ui.custom.CustomTabLayout;
import com.brighteyes.mykora.ui.custom.NonSwipeableViewPager;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MatchInfoActivity extends BaseActivity implements
        MatchInfoFragment.OnFragmentInteractionListener,
        MatchLiveFragment.OnFragmentInteractionListener {

    @BindView(R.id.right_logo)
    ImageView rightLogo;
    @BindView(R.id.left_logo)
    ImageView leftLogo;
    @BindView(R.id.right_name)
    TextView rightName;
    @BindView(R.id.left_name)
    TextView leftName;
    @BindView(R.id.right_goals)
    TextView rightGoals;
    @BindView(R.id.left_goals)
    TextView leftGoals;
    @BindView(R.id.match_state)
    TextView matchState;
    @BindView(R.id.match_start)
    TextView matchTime;

    @BindView(R.id.info_tabs)
    CustomTabLayout tabLayout;
    @BindView(R.id.tabPager)
    NonSwipeableViewPager mViewPager;
    @BindView(R.id.activity_title)
    TextView activityTitle;
    @BindView(R.id.adView)
    RelativeLayout mAdView;
    private PagerAdapter mPagerAdapter;
    private InterstitialAd mInterstitialAd;
    private List<BaseSupportFragment> fragments;
    private List<String> tabsTitles;

    public static Intent buildIntent(Context context, Match match) {
        Intent intent = new Intent(context, MatchInfoActivity.class);
        intent.putExtra(CommonConstants.KEY_MATCH, Parcels.wrap(match));
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_info);
        ButterKnife.bind(this);
        prepareInterstitialAd();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showAd();
            }
        }, 3000);

        activityTitle.setTypeface(mTypeface);
        if (getIntent() != null) {
            Match mMatch = Parcels.unwrap(getIntent().getParcelableExtra(CommonConstants.KEY_MATCH));
            //initHeader(mMatch);
            //initTabsWithViewPager(mMatch);
            addChildEventListener(mMatch);
        }

        loadAd(mAdView);
    }

    private void prepareInterstitialAd() {
        mInterstitialAd = new InterstitialAd(this);
        if (Singleton.getInstance().getAdsIds() != null) {
            mInterstitialAd.setAdUnitId(Singleton.getInstance().getAdsIds().getInterstitialAdId());
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
        }
    }

    private void showAd() {
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    private void addChildEventListener(Match m) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(CommonConstants.FB_NODE_MATCH)
                .orderByKey().equalTo(m.getId())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            Match match = new Match().parseMatch(child);
                            initHeader(match);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void initHeader(Match m) {
        if (rightName != null && leftName != null) {
            rightName.setTypeface(mTypeface);
            leftName.setTypeface(mTypeface);
            matchState.setTypeface(mTypeface);

            switch (m.getMatchStatus()) {
                case CommonConstants.MATCH_STATUS_LIVE:
                    matchState.setBackgroundColor(ContextCompat.getColor(this, R.color.liveRed));
                    break;
                default:
                    matchState.setBackgroundColor(ContextCompat.getColor(this, R.color.finishedDarkGray));
                    break;
            }

            displayImagePicasso(m.getLeftLogo(), leftLogo);
            displayImagePicasso(m.getRightLogo(), rightLogo);
            leftName.setText(m.getLeftName());
            rightName.setText(m.getRightName());
            matchState.setText(m.getMatchStatusString());
            matchTime.setText(m.getTime(mSettingsHelper));
            leftGoals.setText(String.valueOf(m.getLeftScore()));
            rightGoals.setText(String.valueOf(m.getRightScore()));

            initTabsWithViewPager(m);
        }
    }

    private void initTabsWithViewPager(Match m) {
        tabsTitles = Arrays.asList(getResources().getStringArray(R.array.matchInfoTitles));
        fragments = new ArrayList<>();

        switch (m.getMatchStatus()) {
            case CommonConstants.MATCH_STATUS_NOT_STARTED:
            case CommonConstants.MATCH_STATUS_LIVE:
                fragments.add(MatchLiveFragment.newInstance(tabsTitles.get(0), m));
                fragments.add(MatchInfoFragment.newInstance(tabsTitles.get(1), m));
                break;
            case CommonConstants.MATCH_STATUS_FINISHED:
                fragments.add(FinishedMatchFragment.newInstance(m));
                tabLayout.setVisibility(View.GONE);
                break;
        }

        mPagerAdapter =
                new PagerAdapter(getSupportFragmentManager(), fragments);

        addDividers(tabLayout);

        mViewPager.setAdapter(mPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);
        // Select Match info tab at beginning
        if (tabLayout.getTabCount() > 1) {
            TabLayout.Tab tab = tabLayout.getTabAt(1);
            tab.select();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

    @OnClick(R.id.ic_home_action)
    public void onClickHome() {
        mNavigator.navigateToMainActivity();
    }


    @OnClick(R.id.ic_time)
    public void onClickTime() {
        mNavigator.navigateToTimeZonesActivity();
    }

    @OnClick(R.id.ic_notifications)
    public void onClickNotifications() {
        mNavigator.navigateToNotificationActivity();
    }


}