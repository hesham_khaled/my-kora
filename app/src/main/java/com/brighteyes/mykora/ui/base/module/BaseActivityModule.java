package com.brighteyes.mykora.ui.base.module;

import com.brighteyes.mykora.ui.base.ActivityScope;
import com.brighteyes.mykora.ui.base.BaseActivity;
import com.brighteyes.mykora.ui.helper.ActivityHelper;

import dagger.Module;
import dagger.Provides;

@Module
public class BaseActivityModule {

    private BaseActivity baseActivity;

    public BaseActivityModule(BaseActivity splashActivity) {
        this.baseActivity = splashActivity;
    }

    @Provides
    @ActivityScope
    BaseActivity provideBaseActivity() {
        return baseActivity;
    }

    @Provides
    @ActivityScope
    ActivityHelper provideActivityHelper() {
        return ActivityHelper.createInstance(baseActivity);
    }
}
