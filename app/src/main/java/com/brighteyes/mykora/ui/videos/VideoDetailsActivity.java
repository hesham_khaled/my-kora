package com.brighteyes.mykora.ui.videos;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brighteyes.mykora.R;
import com.brighteyes.mykora.Singleton;
import com.brighteyes.mykora.entities.Video;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.helper.UIHelper;
import com.brighteyes.mykora.ui.base.BaseActivity;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.EMVideoView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTouch;

import static com.brighteyes.mykora.helper.CommonConstants.MEDIA_TYPE_FRAME;
import static com.brighteyes.mykora.helper.CommonConstants.MEDIA_TYPE_URL;

public class VideoDetailsActivity extends BaseActivity implements OnPreparedListener {

    @BindView(R.id.activity_title)
    TextView activityTitle;
    @BindView(R.id.video_view)
    EMVideoView videoView;
    @BindView(R.id.video_frame)
    WebView webView;
    @BindView(R.id.customViewContainer)
    FrameLayout customViewContainer;
    @BindView(R.id.invalid_video)
    TextView invalidVideo;
    @BindView(R.id.video_title)
    TextView videoTitle;
    @BindView(R.id.video_description)
    TextView description;
    @BindView(R.id.share_label)
    TextView share;
    @BindView(R.id.recommendedTV)
    TextView recommendedTv;
    @BindView(R.id.adView)
    RelativeLayout mAdView;
    @BindView(R.id.recommended_rv)
    RecyclerView recommendedRV;
    private WebChromeClient.CustomViewCallback customViewCallback;
    private View mCustomView;
    private myWebChromeClient mWebChromeClient;
    private myWebViewClient webViewClient;

    private Video mVideo;
    private VideosAdapter mRecommendedAdapter;
    private List<Video> recommendedVideos = new ArrayList<>();
    private int recommendedCount = 0;
    private InterstitialAd mInterstitialAd;

    public static Intent buildIntent(Context context, Video video) {
        Intent intent = new Intent(context, VideoDetailsActivity.class);
        intent.putExtra(CommonConstants.FB_NODE_VIDEO, Parcels.wrap(video));
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_details);
        prepareInterstitialAd();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showAd();
            }
        }, 3000);

        activityTitle.setTypeface(mTypeface);
        activityTitle.setText(R.string.videos);
        videoTitle.setTypeface(mTypeface);
        description.setTypeface(mTypeface);
        share.setTypeface(mTypeface);
        recommendedTv.setTypeface(mTypeface);

        if (getIntent() != null) {
            mVideo = Parcels.unwrap(getIntent().getParcelableExtra(CommonConstants.FB_NODE_VIDEO));
            if (mVideo != null) {
                switch (mVideo.getMediaType()) {
                    case MEDIA_TYPE_URL:
                        initVideoPlayer();
                        break;
                    case MEDIA_TYPE_FRAME:
                        initWebView();
                        break;
                    case -1:
                        invalidVideo.setVisibility(View.VISIBLE);
                        break;
                }
            }
        }

    }


    private void prepareInterstitialAd() {
        mInterstitialAd = new InterstitialAd(this);
        if (Singleton.getInstance().getAdsIds() != null) {
            mInterstitialAd.setAdUnitId(Singleton.getInstance().getAdsIds().getInterstitialAdId());
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
        }
    }

    private void showAd() {
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (videoView != null && videoView.isPlaying()) {
//            videoView.stopPlayback();
            videoView.pause();
        }
        if (webView != null) {
            webView.onPause();
        }
    }

    @Override
    protected void onResume() {

        if (videoView != null && videoView.isPlaying()) {
            videoView.restart();
        }

        if (webView != null) {
            webView.onResume();
        }

        super.onResume();
    }

    private void initVideoPlayer() {
        initVideoView();
        addFullScreenButton();
        bindVideoData(mVideo);
    }


    private void initWebView() {
        bindVideoData(mVideo);
        webView.setVisibility(View.VISIBLE);
        customViewContainer.setVisibility(View.VISIBLE);

        webViewClient = new myWebViewClient();
        webView.setWebViewClient(webViewClient);

        mWebChromeClient = new myWebChromeClient();
        webView.setWebChromeClient(mWebChromeClient);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSaveFormData(true);
        webView.loadUrl(mVideo.getVideoIFrame());
    }

    private void initVideoView() {
        videoView.setVisibility(View.VISIBLE);
        if (videoView != null) {
            if (videoView.isPlaying()) {
                videoView.stopPlayback();
            }
            videoView.setReleaseOnDetachFromWindow(true);
            videoView.setPreviewImage(Uri.parse(mVideo.getThumb_url()));
            videoView.setVideoURI(Uri.parse(mVideo.getVideoUrl()));
            videoView.setOnPreparedListener(this);
        }
        loadAd(mAdView);
        addChildValueListener();
    }

    private void addFullScreenButton() {
        Button fullScreenBtn = new Button(this);
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(32, 32);
        int dp8 = UIHelper.convertToPx(8, this);
        llp.setMarginStart(dp8);
        fullScreenBtn.setPadding(dp8, 0, dp8, dp8);
        fullScreenBtn.setLayoutParams(llp);
        fullScreenBtn.setBackground(ContextCompat.getDrawable(this, R.drawable.ic_full));
        fullScreenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mNavigator.navigateToFullScreen(mVideo, videoView.getCurrentPosition(), videoView.isPlaying());
            }
        });

        videoView.getVideoControls().addExtraView(fullScreenBtn);
    }

    private void bindVideoData(Video video) {
        if (videoTitle != null && description != null) {
            videoTitle.setText(video.getName());
            description.setText(video.getDescription());
            initRelatedVideos(video);
        }
    }

    private void addChildValueListener() {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(CommonConstants.FB_NODE_VIDEO)
                .orderByKey().equalTo(mVideo.getId())
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        Video video = new Video().parseVideo(dataSnapshot);
                        if (video.getVideoUrl() != mVideo.getVideoUrl()) {
                            initVideoView();
                        }
                        bindVideoData(video);
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        finish();
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void initRelatedVideos(final Video video) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(CommonConstants.FB_NODE_VIDEO)
                .orderByChild(CommonConstants.FB_VID_TAGS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        recommendedVideos.clear();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            Video newVideo = new Video().parseVideo(child);
                            if (!newVideo.getId().equals(video.getId()) && haveCommonTag(video, newVideo)) {
                                recommendedVideos.add(newVideo);
                                updateRecommendedVideos(CommonConstants.FLAG_INITIALIZE, -1);
                            }
                        }

                        if (!recommendedVideos.isEmpty()) {
                            recommendedTv.setVisibility(View.VISIBLE);
                            recommendedRV.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private boolean haveCommonTag(Video video1, Video video2) {

        String[] vid1array = video1.getTags().split(",");
        for (int i = 0; i < vid1array.length; i++) {
            vid1array[i] = vid1array[i].trim();
        }
        Set<String> video1tagsSet = new HashSet<>(Arrays.asList(vid1array));


        String[] vid2array = video2.getTags().split(",");
        for (int i = 0; i < vid2array.length; i++) {
            vid2array[i] = vid2array[i].trim();
        }
        Set<String> video2tagsSet = new HashSet<>(Arrays.asList(vid2array));

        video1tagsSet.retainAll(video2tagsSet);
        return video1tagsSet.size() > 0;
    }

    public void updateRecommendedVideos(int transaction, int itemPosition) {
        switch (transaction) {
            case CommonConstants.FLAG_INITIALIZE:
                initRecommended();
                try {
                    addRecommendedChildValueListener();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                break;
            case CommonConstants.FLAG_INSERT:
                mRecommendedAdapter.notifyItemInserted(recommendedVideos.size() - 1);
                break;
            case CommonConstants.FLAG_UPDATE:
                mRecommendedAdapter.notifyItemChanged(itemPosition);
                break;
            case CommonConstants.FLAG_DELETE:
                mRecommendedAdapter.notifyItemRemoved(itemPosition);
                break;
        }
    }

    private void addRecommendedChildValueListener() {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(CommonConstants.FB_NODE_VIDEO)
                .orderByChild(CommonConstants.FB_VID_CAT_ID).equalTo(mVideo.getCategoryID())
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        recommendedCount++;
                        if (recommendedCount > recommendedVideos.size()) {
                            Video video = new Video().parseVideo(dataSnapshot);
                            if (!video.getId().equals(video.getId())) {
                                recommendedVideos.add(video);
                                updateRecommendedVideos(CommonConstants.FLAG_INSERT, recommendedVideos.size() - 1);
                            }
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        Video video = new Video().parseVideo(dataSnapshot);
                        int position = getVideoPosition(video.getId());
                        if (!video.getId().equals(video.getId())) {
                            recommendedVideos.set(position, video);
                            updateRecommendedVideos(CommonConstants.FLAG_UPDATE, position);
                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        int position = getVideoPosition(dataSnapshot.getKey());
                        recommendedVideos.remove(position);
                        updateRecommendedVideos(CommonConstants.FLAG_DELETE, position);
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void initRecommended() {
        recommendedRV.setHasFixedSize(true);

        mRecommendedAdapter = new VideosAdapter(this, recommendedVideos, true, mNavigator);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false);
        recommendedRV.setLayoutManager(layoutManager);
        recommendedRV.setAdapter(mRecommendedAdapter);
    }

    private int getVideoPosition(String id) {
        int index = -1;
        for (Video video : recommendedVideos) {
            if (!video.getId().equals("") && video.getId().equals(id)) {
                index = recommendedVideos.indexOf(video);
                break;
            }
        }
        return index;
    }

    @OnClick(R.id.ic_time)
    public void onclickTime() {
        mNavigator.navigateToTimeZonesActivity();
    }

    @OnClick(R.id.ic_notifications)
    public void onclickNotification() {
        mNavigator.navigateToNotificationActivity();
    }

    @OnClick(R.id.share)
    public void share() {
        mNavigator.shareVideo(mVideo);
    }

    @OnClick(R.id.ic_home_action)
    public void goHome() {
        mNavigator.navigateToMainActivity();
    }

    protected void loadAd(AdView adView) {
        try {
            if (Singleton.getInstance().getAdsIds() != null) {
                adView.setAdUnitId(Singleton.getInstance().getAdsIds().getBannerAdId());
            }
            AdRequest adRequest = new AdRequest.Builder().build();
            adView.loadAd(adRequest);
        } catch (Exception ex) {
            Log.d(CommonConstants.TAG, "Couldn't load ad, " + ex.getLocalizedMessage());
        }
    }


    @OnTouch(R.id.video_title)
    public boolean onTouchTitle() {
        videoTitle.setSelected(true);
        return false;
    }

    @Override
    public void onPrepared() {
        videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (videoView != null) {
                    videoView.start();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CommonConstants.FULL_SCREEN_CODE && resultCode == RESULT_OK) {
            int position = data.getIntExtra(CommonConstants.CURRENT_POSITION, 0);
            boolean isPlaying = data.getBooleanExtra(CommonConstants.IS_PLAYING, true);
            if (videoView != null && position > 0) {
                videoView.seekTo(position);
                if (isPlaying) {
                    videoView.start();
                }
            }
        }
    }

//    @Override
//    protected void onPause() {
//        super.onPause();
//        videoView.pause();
//    }


    class myWebChromeClient extends WebChromeClient {
        //private Bitmap mDefaultVideoPoster;
        private View mVideoProgressView;

        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
            onShowCustomView(view, callback);
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {

            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
            mCustomView = view;
            webView.setVisibility(View.GONE);
            customViewContainer.setVisibility(View.VISIBLE);
            customViewContainer.addView(view);
            customViewCallback = callback;
        }

        @Override
        public View getVideoLoadingProgressView() {

            if (mVideoProgressView == null) {
                LayoutInflater inflater = LayoutInflater.from(VideoDetailsActivity.this);
                mVideoProgressView = inflater.inflate(R.layout.video_progress, null);
            }
            return mVideoProgressView;
        }

        @Override
        public void onHideCustomView() {
            super.onHideCustomView();
            if (mCustomView == null) {
                return;
            }

            webView.setVisibility(View.VISIBLE);
            customViewContainer.setVisibility(View.GONE);

            // Hide the custom view.
            mCustomView.setVisibility(View.GONE);

            // Remove the custom view from its container.
            customViewContainer.removeView(mCustomView);
            customViewCallback.onCustomViewHidden();

            mCustomView = null;
        }
    }

    class myWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);
        }
    }
}