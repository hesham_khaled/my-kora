package com.brighteyes.mykora.ui.custom;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.brighteyes.mykora.R;
import com.brighteyes.mykora.entities.Video;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.helper.UIHelper;
import com.brighteyes.mykora.ui.base.BaseActivity;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.EMVideoView;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FullScreenVideo extends BaseActivity implements OnPreparedListener {

    @BindView(R.id.em_video_view)
    EMVideoView emVideoView;
    private Video mVideo;
    private int mPosition;
    private boolean isPlaying;

    public static Intent buildIntent(Context context, Video video, int currentPosition, boolean isPlaying) {
        Intent intent = new Intent(context, FullScreenVideo.class);
        intent.putExtra(CommonConstants.FB_NODE_VIDEO, Parcels.wrap(video));
        intent.putExtra(CommonConstants.CURRENT_POSITION, currentPosition);
        intent.putExtra(CommonConstants.IS_PLAYING, isPlaying);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_video);
        ButterKnife.bind(this);

        if (getIntent() != null && getIntent().hasExtra(CommonConstants.FB_NODE_VIDEO)) {
            mVideo = Parcels.unwrap(getIntent().getParcelableExtra(CommonConstants.FB_NODE_VIDEO));
            mPosition = getIntent().getIntExtra(CommonConstants.CURRENT_POSITION, 0);
            isPlaying = getIntent().getBooleanExtra(CommonConstants.IS_PLAYING, true);
            initEMVideo();
        }

    }

    private void initEMVideo() {
        if (emVideoView != null) {

            Button exitBtn = new Button(this);
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(32, 32);
            int dp8 = UIHelper.convertToPx(8, this);
            llp.setMarginStart(dp8);
            exitBtn.setPadding(dp8, 0, dp8, dp8);
            exitBtn.setLayoutParams(llp);
            exitBtn.setBackground(ContextCompat.getDrawable(this, R.drawable.ic_full_exit));
            exitBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    exitFullScreen();
                }
            });

            emVideoView.getVideoControls().addExtraView(exitBtn);
            emVideoView.setReleaseOnDetachFromWindow(true);
            emVideoView.setPreviewImage(Uri.parse(mVideo.getThumb_url()));
            emVideoView.setVideoURI(Uri.parse(mVideo.getVideoUrl()));
            emVideoView.setOnPreparedListener(this);
        }
    }

    private void exitFullScreen() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(CommonConstants.CURRENT_POSITION, emVideoView.getCurrentPosition());
        resultIntent.putExtra(CommonConstants.IS_PLAYING, emVideoView.isPlaying());
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    @Override
    public void onPrepared() {
        if (emVideoView != null && mPosition > 0) {
            emVideoView.seekTo(mPosition);
            if (isPlaying) {
                emVideoView.start();
            }
        }
    }

    @Override
    public void onBackPressed() {
        exitFullScreen();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitFullScreen();
        }
        return super.onKeyDown(keyCode, event);
    }

}
