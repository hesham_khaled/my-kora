package com.brighteyes.mykora.entities;

import com.google.firebase.database.DataSnapshot;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shanan on 11/02/2017.
 */

@Parcel
public class ChannelMedia {
    String id;
    List<MediaSource> mediaSourceList = new ArrayList<>();

    public ChannelMedia() {

    }

    public ChannelMedia parseMediaSources(DataSnapshot dataSnapshot) {
        this.id = dataSnapshot.getKey();
        //this.mediaSourceList = (String) dataSnapshot.child(CommonConstants.FB_CHAMP_LOGO_URL).getValue();
        return this;
    }

    public List<MediaSource> getMatches() {
        return mediaSourceList;
    }

    public void setMediaSources(List<MediaSource> mediaSources) {
        this.mediaSourceList = mediaSources;
    }

    public String getId() {
        return id;
    }

}
