package com.brighteyes.mykora.ui.news;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.brighteyes.mykora.R;
import com.brighteyes.mykora.Singleton;
import com.brighteyes.mykora.entities.News;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.ui.base.BaseActivity;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;

public class NewsDetailsActivity extends BaseActivity {

    @BindView(R.id.activity_title)
    TextView activityTitle;

    @BindView(R.id.thumb)
    ImageView newsImage;
    @BindView(R.id.news_title)
    TextView newsTitle;
    @BindView(R.id.news_description)
    TextView description;
    @BindView(R.id.share_label)
    TextView share;
    @BindView(R.id.recommendedTV)
    TextView recommendedTv;
    @BindView(R.id.adView)
    AdView mAdView;
    @BindView(R.id.recommended_rv)
    RecyclerView recommendedRV;

    private News mNews;
    private RelatedNewsAdapter mRelatedAdapter;
    private List<News> recommendedNews = new ArrayList<>();
    private int recommendedCount = 0;

    public static Intent buildIntent(Context context, News news) {
        Intent intent = new Intent(context, NewsDetailsActivity.class);
        intent.putExtra(CommonConstants.FB_NODE_NEWS, Parcels.wrap(news));
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);

        activityTitle.setTypeface(mTypeface);
        activityTitle.setText(R.string.news);
        newsTitle.setTypeface(mTypeface);
        description.setTypeface(mTypeface);
        share.setTypeface(mTypeface);
        recommendedTv.setTypeface(mTypeface);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent() != null) {
            mNews = Parcels.unwrap(getIntent().getParcelableExtra(CommonConstants.FB_NODE_NEWS));

            if (mNews != null) {
                bindNewsData(mNews);
                addChildValueListener();
            }

        }
    }

    private void bindNewsData(News news) {
        if (newsImage != null && newsTitle != null && description != null && mAdView != null) {
            displayImagePicasso(news.getPhoto_url(), newsImage);
            newsTitle.setText(news.getTitle());
            description.setText(news.getDetails());
            loadAd(mAdView);
            initRelatedNews(news);
        }
    }

    private void addChildValueListener() {

        FirebaseDatabase.getInstance()
                .getReference()
                .child(CommonConstants.FB_NODE_NEWS)
                .orderByKey().equalTo(mNews.getId())
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        mNews = new News().parseNews(dataSnapshot);
                        bindNewsData(mNews);
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void initRelatedNews(final News news) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(CommonConstants.FB_NODE_NEWS)
                .orderByChild(CommonConstants.FB_NEWS_TAGS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        recommendedNews.clear();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            News newsItem = new News().parseNews(child);
                            if (!newsItem.getId().equals(news.getId()) && haveCommonTag(news, newsItem)) {
                                recommendedNews.add(newsItem);
                                updateRecommendedNews(newsItem, CommonConstants.FLAG_INITIALIZE, -1);
                            }
                        }
                        if (!recommendedNews.isEmpty()) {
                            recommendedTv.setVisibility(View.VISIBLE);
                            recommendedRV.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void updateRecommendedNews(News news, int transaction, int itemPosition) {
        switch (transaction) {
            case CommonConstants.FLAG_INITIALIZE:
                initRecommended();
                try {
                    addRecommendedChildValueListener(news);
                } catch (Exception ex) {
                    Log.d(CommonConstants.TAG, ex.getLocalizedMessage());
                }

                break;
            case CommonConstants.FLAG_INSERT:
                mRelatedAdapter.notifyItemInserted(recommendedNews.size() - 1);
                break;
            case CommonConstants.FLAG_UPDATE:
                mRelatedAdapter.notifyItemChanged(itemPosition);
                break;
            case CommonConstants.FLAG_DELETE:
                mRelatedAdapter.notifyItemRemoved(itemPosition);
                break;
        }
    }

    private void addRecommendedChildValueListener(final News news) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(CommonConstants.FB_NODE_NEWS)
                .orderByChild(CommonConstants.FB_NEWS_CATEGORY_ID).equalTo(news.getCatId())
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        recommendedCount++;
                        if (recommendedCount > recommendedNews.size()) {
                            News newsItem = new News().parseNews(dataSnapshot);
                            if (!newsItem.getId().equals(news.getId())) {
                                recommendedNews.add(newsItem);
                                updateRecommendedNews(newsItem, CommonConstants.FLAG_INSERT, recommendedNews.size() - 1);
                            }
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        News newsItem = new News().parseNews(dataSnapshot);
                        int position = getNewsPosition(newsItem.getId());
                        if (!newsItem.getId().equals(news.getId())) {
                            recommendedNews.set(position, newsItem);
                            updateRecommendedNews(news, CommonConstants.FLAG_UPDATE, position);
                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        int position = getNewsPosition(dataSnapshot.getKey());
                        recommendedNews.remove(position);
                        updateRecommendedNews(news, CommonConstants.FLAG_DELETE, position);
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void initRecommended() {
        recommendedRV.setHasFixedSize(true);

        mRelatedAdapter = new RelatedNewsAdapter(this, recommendedNews, mNavigator);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false);
        recommendedRV.setLayoutManager(layoutManager);
        recommendedRV.setAdapter(mRelatedAdapter);
    }

    private int getNewsPosition(String id) {
        int index = -1;
        for (News news : recommendedNews) {
            if (!news.getId().equals("") && news.getId().equals(id)) {
                index = recommendedNews.indexOf(news);
                break;
            }
        }
        return index;
    }

    @OnClick(R.id.ic_time)
    public void onclickTime() {
        mNavigator.navigateToTimeZonesActivity();
    }

    @OnClick(R.id.ic_notifications)
    public void onclickNotification() {
        mNavigator.navigateToNotificationActivity();
    }

    @OnClick(R.id.share)
    public void share() {
        mNavigator.shareNewsItem(mNews);
    }

    @OnClick(R.id.ic_home_action)
    public void goHome() {
        mNavigator.navigateToMainActivity();
    }

    protected void loadAd(AdView adView) {
        if (Singleton.getInstance().getAdsIds() != null) {
            adView.setAdUnitId(Singleton.getInstance().getAdsIds().getBannerAdId());
        }
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    private boolean haveCommonTag(News news1, News news2) {
        String[] news1array = news1.getTags().split(",");
        for (int i = 0; i < news1array.length; i++) {
            news1array[i] = news1array[i].trim();
        }
        Set<String> news1tagsSet = new HashSet<>(Arrays.asList(news1array));


        String[] news2array = news2.getTags().split(",");
        for (int i = 0; i < news2array.length; i++) {
            news2array[i] = news2array[i].trim();
        }
        Set<String> news2tagsSet = new HashSet<>(Arrays.asList(news2array));

        news1tagsSet.retainAll(news2tagsSet);
        return news1tagsSet.size() > 0;
    }
}
