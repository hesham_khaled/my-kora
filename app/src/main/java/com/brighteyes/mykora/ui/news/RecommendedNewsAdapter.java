package com.brighteyes.mykora.ui.news;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brighteyes.mykora.App;
import com.brighteyes.mykora.R;
import com.brighteyes.mykora.entities.News;
import com.brighteyes.mykora.nav.Navigator.Navigator;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shanan on 30/01/2017.
 */

public class RecommendedNewsAdapter
        extends RecyclerView.Adapter<RecommendedNewsAdapter.ViewHolder> {

    private Activity mContext;
    private List<News> newsList;
    private Typeface typeface;
    private Navigator navigator;

    public RecommendedNewsAdapter(Activity context, List<News> newsList, Navigator navigator) {
        mContext = context;
        this.newsList = newsList;
        this.navigator = navigator;
        typeface = App.defaultFont;
    }

    @Override
    public RecommendedNewsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.recommended_video_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final News news = newsList.get(position);

        if (news != null) {
            bindData(holder, news);
        }
    }

    private void bindData(final ViewHolder holder, final News news) {
        Picasso.with(mContext)
                .load(news.getPhoto_url())
                .fit()
                .placeholder(R.drawable.logo_transparent)
                .into(holder.thumb);
        holder.title.setText(news.getTitle());

        holder.thumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigator.navigateToNewsDetailsActivity(news);
            }
        });


    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.thumb)
        ImageView thumb;
        @BindView(R.id.play)
        ImageView play;
        @BindView(R.id.video_title)
        TextView title;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            title.setTypeface(typeface);
            play.setVisibility(View.GONE);
        }
    }
}