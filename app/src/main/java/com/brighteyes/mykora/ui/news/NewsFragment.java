package com.brighteyes.mykora.ui.news;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.brighteyes.mykora.R;
import com.brighteyes.mykora.entities.Category;
import com.brighteyes.mykora.entities.News;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.helper.Utils;
import com.brighteyes.mykora.ui.base.BaseSupportFragment;
import com.brighteyes.mykora.ui.base.PagerAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewsFragment extends BaseSupportFragment {
    @BindView(R.id.recommended_layout)
    ViewGroup recommendedLayout;
    @BindView(R.id.recommendedTV)
    TextView recommendedTV;
    @BindView(R.id.recommended_rv)
    RecyclerView recommendedRV;
    @BindView(R.id.categories_viewpager)
    ViewPager mainViewPager;
    @BindView(R.id.content_tabs)
    TabLayout tabLayout;

    @BindView(R.id.retry)
    TextView retry;
    @BindView(R.id.tvMessage)
    TextView tvMessage;
    @BindView(R.id.tryAgainLayout)
    ViewGroup tryAgainLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    List<BaseSupportFragment> fragments = new ArrayList<>();
    ArrayList<String> ids = new ArrayList<>();
    private PagerAdapter mPagerAdapter;
    private RecommendedNewsAdapter mRecommendedAdapter;
    private List<News> recommendedNews = new ArrayList<>();
    private int recommendedCount = 0;
    private int catsCount = 0;

    public NewsFragment() {
        // Required empty public constructor
    }

    public static NewsFragment newInstance(String fragName) {
        NewsFragment fragment = new NewsFragment();
        fragment.setTitle(fragName);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_videos, container, false);

        ButterKnife.bind(this, rootView);

        recommendedTV.setTypeface(mTypeface);

        addDividers(tabLayout);

        getCategories();

        getRecommendedNews();

        return rootView;
    }

    private void getCategories() {

        try {
            if (!Utils.getNetworkState(getActivity())) {
                showTryAgainLayout(R.string.connection_error);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        FirebaseDatabase.getInstance()
                .getReference()
                .child(CommonConstants.FB_NODE_NEWS_CATEGORY)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        fragments.clear();
                        ids.clear();
                        updateTabs(CommonConstants.FLAG_INITIALIZE);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void updateTabs(int transaction) {
        switch (transaction) {
            case CommonConstants.FLAG_INITIALIZE:
                initTabs();
                try {
                    addCategoriesChildValueListener();
                } catch (Exception ex) {
                    Log.d(CommonConstants.TAG, ex.getLocalizedMessage());
                }
                break;
            case CommonConstants.FLAG_INSERT:
                mPagerAdapter.notifyDataSetChanged();
                break;
            case CommonConstants.FLAG_UPDATE:
                mPagerAdapter.notifyDataSetChanged();
                break;
            case CommonConstants.FLAG_DELETE:
                mPagerAdapter.notifyDataSetChanged();
                break;
        }
    }

    private void addCategoriesChildValueListener() {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(CommonConstants.FB_NODE_NEWS_CATEGORY)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        Category category = new Category().parseCategory(dataSnapshot);
                        if (ids.indexOf(category.getId()) == -1) {
                            fragments.add(CategoryNewsFragment.newInstance(category));
                            ids.add(category.getId());
                            updateTabs(CommonConstants.FLAG_INSERT);
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        Category category = new Category().parseCategory(dataSnapshot);
                        if (ids.contains(category.getId())) {
                            int position = ids.indexOf(category.getId());
                            fragments.set(position, new CategoryNewsFragment().newInstance(category));
                            ids.set(position, category.getId());
                            updateTabs(CommonConstants.FLAG_UPDATE);
                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        Category category = new Category().parseCategory(dataSnapshot);

                        if (ids.indexOf(category.getId()) != -1) {
                            int tab = ids.indexOf(category.getId());
                            fragments.remove(tab);
                            ids.remove(tab);
                            updateTabs(CommonConstants.FLAG_DELETE);
                        }

                        if (fragments.size() == 0) {
                            showTryAgainLayout(R.string.no_news);
                        }
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
    }

    private void initTabs() {

        mPagerAdapter = new PagerAdapter(getChildFragmentManager(), fragments);
        mainViewPager.setAdapter(mPagerAdapter);
        tabLayout.setupWithViewPager(mainViewPager);
    }

    private void getRecommendedNews() {

        FirebaseDatabase.getInstance()
                .getReference()
                .child(CommonConstants.FB_NODE_NEWS)
                .orderByChild(CommonConstants.FB_VID_IS_IMPORTANT).equalTo(true)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        recommendedNews.clear();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            recommendedNews.add(new News().parseNews(child));
                        }
                        if (recommendedNews.size() > 0) {
                            recommendedLayout.setVisibility(View.VISIBLE);
                            updateRecommendedNews(CommonConstants.FLAG_INITIALIZE, -1);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void updateRecommendedNews(int transaction, int itemPosition) {
        switch (transaction) {
            case CommonConstants.FLAG_INITIALIZE:
                initRecommended();
                try {
                    addRecommendedChildValueListener();
                } catch (Exception ex) {
                    Log.d(CommonConstants.TAG, ex.getLocalizedMessage());
                }

                break;
            case CommonConstants.FLAG_INSERT:
                mRecommendedAdapter.notifyItemInserted(recommendedNews.size() - 1);
                break;
            case CommonConstants.FLAG_UPDATE:
                mRecommendedAdapter.notifyItemChanged(itemPosition);
                break;
            case CommonConstants.FLAG_DELETE:
                mRecommendedAdapter.notifyItemRemoved(itemPosition);
                break;
        }
    }

    private void addRecommendedChildValueListener() {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(CommonConstants.FB_NODE_NEWS)
                .orderByChild(CommonConstants.FB_NEWS_IS_IMPORTANT).equalTo(true)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        recommendedCount++;
                        if (recommendedCount > recommendedNews.size()) {
                            News news = new News().parseNews(dataSnapshot);
                            recommendedNews.add(news);
                            updateRecommendedNews(CommonConstants.FLAG_INSERT, recommendedNews.size() - 1);
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        News news = new News().parseNews(dataSnapshot);
                        int position = getNewsPosition(news.getId());
                        recommendedNews.set(position, news);
                        updateRecommendedNews(CommonConstants.FLAG_UPDATE, position);
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        int position = getNewsPosition(dataSnapshot.getKey());
                        recommendedNews.remove(position);
                        updateRecommendedNews(CommonConstants.FLAG_DELETE, position);
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void initRecommended() {
        recommendedRV.setHasFixedSize(true);

        int spanCount;
        if (recommendedNews.size() == 1) {
            spanCount = 1;
        } else if (recommendedNews.size() == 2) {
            spanCount = 2;
        } else {
            spanCount = 3;
        }

        mRecommendedAdapter = new RecommendedNewsAdapter(getActivity(), recommendedNews, mNavigator);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(),
                spanCount, LinearLayoutManager.HORIZONTAL, false);
        recommendedRV.setLayoutManager(layoutManager);
        recommendedRV.setAdapter(mRecommendedAdapter);
    }

    private int getNewsPosition(String id) {
        int index = -1;
        for (News news : recommendedNews) {
            if (!news.getId().equals("") && news.getId().equals(id)) {
                index = recommendedNews.indexOf(news);
                break;
            }
        }
        return index;
    }

    @OnClick(R.id.retry)
    public void Onclick() {
        hideTryAgainLayout();
        getCategories();
    }

    private void showTryAgainLayout(int resId) {
        if (tryAgainLayout != null) {
            tryAgainLayout.setVisibility(View.VISIBLE);
            tabLayout.setVisibility(View.GONE);
            mainViewPager.setVisibility(View.GONE);
            tvMessage.setTypeface(mTypeface);
            retry.setTypeface(mTypeface);
            tvMessage.setText(resId);
        }
    }

    private void hideTryAgainLayout() {
        if (tryAgainLayout != null) {
            tryAgainLayout.setVisibility(View.GONE);
            tabLayout.setVisibility(View.VISIBLE);
            mainViewPager.setVisibility(View.VISIBLE);

        }
    }

    private void showProgressBar(boolean show) {
        if (progressBar != null) {
            if (show) {
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setIndeterminate(show);
            } else {
                progressBar.setVisibility(View.GONE);
            }
        }
    }
}
