package com.brighteyes.mykora.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brighteyes.mykora.R;
import com.brighteyes.mykora.fcm.FirebaseHelper;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.helper.Utils;
import com.brighteyes.mykora.ui.base.BaseActivity;
import com.brighteyes.mykora.ui.base.BaseSupportFragment;
import com.brighteyes.mykora.ui.base.PagerAdapter;
import com.brighteyes.mykora.ui.custom.CustomTabLayout;
import com.brighteyes.mykora.ui.custom.NonSwipeableViewPager;
import com.brighteyes.mykora.ui.matches.MatchesFragment;
import com.brighteyes.mykora.ui.settings.TimeZoneActivity;
import com.brighteyes.mykora.ui.videos.VideosFragment;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @BindView(R.id.ic_time)
    ImageView timeIcon;
    @BindView(R.id.ic_notifications)
    ImageView notificationsIcon;
    @BindView(R.id.activity_title)
    TextView activityTitle;
    @BindView(R.id.viewPager)
    NonSwipeableViewPager mViewPager;
    @BindView(R.id.tabs)
    CustomTabLayout tabLayout;
    @BindView(R.id.adView)
    RelativeLayout mAdView;
    @BindView(R.id.progress_view)
    CircularProgressView progressView;

    private PagerAdapter mSectionsPagerAdapter;

    public static Intent buildIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (mSharedPreferencesHelper.getBoolean(CommonConstants.KEY_FIRST_RUN, true)) {
            onClickTime();
            mSharedPreferencesHelper.setBoolean(CommonConstants.KEY_FIRST_RUN, false);
        }

        activityTitle.setTypeface(mTypeface);
        activityTitle.setText(R.string.today_matches);

        setupView();

        if (Utils.getNetworkState(this)) {
            try {
                loadAd(mAdView);
            } catch (Exception e) {
            }
        }
    }

    private void setupView() {
        if (FirebaseInstanceId.getInstance().getToken() == null) {
            FirebaseHelper.sendRegistrationToServer(this, FirebaseInstanceId.getInstance().getToken());
        }

        List<String> tabsTitles = Arrays.asList(getResources().getStringArray(R.array.mainTabsTitles));

        List<BaseSupportFragment> fragments = new ArrayList<>();

        fragments.add(MatchesFragment.newInstance(tabsTitles.get(0)));
        fragments.add(VideosFragment.newInstance(tabsTitles.get(1)));
//        fragments.add(NewsFragment.newInstance(tabsTitles.get(2)));

        mSectionsPagerAdapter =
                new PagerAdapter(getSupportFragmentManager(), fragments);

        addDividers(tabLayout);

        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);
    }

    @OnClick(R.id.ic_time)
    public void onClickTime() {
        Intent intent = TimeZoneActivity.buildIntent(this);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            setupView();
        }
    }

    @OnClick(R.id.ic_notifications)
    public void onClickNotifications() {
        mNavigator.navigateToNotificationActivity();
    }


}