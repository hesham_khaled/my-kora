package com.brighteyes.mykora.ui.matchInfo;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brighteyes.mykora.R;
import com.brighteyes.mykora.entities.Match;
import com.brighteyes.mykora.ui.base.BaseSupportFragment;
import com.google.android.gms.ads.AdView;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MatchInfoFragment extends BaseSupportFragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_MATCH = "match";
    @BindView(R.id.championship_tv)
    TextView championshipTv;
    @BindView(R.id.league_icon)
    ImageView championshipLogo;
    @BindView(R.id.stadium_tv)
    TextView stadiumTv;
    @BindView(R.id.referee_tv)
    TextView refereeTv;
    @BindView(R.id.commentator_tv)
    TextView commentatorTv;
    @BindView(R.id.channel_tv)
    TextView channelTv;
    @BindView(R.id.adView)
    AdView mAdView;
    private Match mMatch;
    private OnFragmentInteractionListener mListener;

    public MatchInfoFragment() {
        // Required empty public constructor
    }

    public static MatchInfoFragment newInstance(String title, Match match) {
        MatchInfoFragment fragment = new MatchInfoFragment();
        fragment.setTitle(title);
        Bundle args = new Bundle();
        args.putParcelable(ARG_MATCH, Parcels.wrap(match));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mMatch = Parcels.unwrap(getArguments().getParcelable(ARG_MATCH));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_match_info, container, false);
        ButterKnife.bind(this, rootView);

        championshipTv.setTypeface(mTypeface);
        stadiumTv.setTypeface(mTypeface);
        refereeTv.setTypeface(mTypeface);
        commentatorTv.setTypeface(mTypeface);
        channelTv.setTypeface(mTypeface);

        loadAd(mAdView);
        bindMatchData();
        return rootView;
    }

    private void bindMatchData() {
        championshipTv.setText(mMatch.getChampName());
//        displayImagePicasso(mMatch.getLeftLogo(), championshipLogo);
        stadiumTv.setText(mMatch.getStadName());
        refereeTv.setText(mMatch.getRefereeName());
        commentatorTv.setText(mMatch.getCommentatorName());
        channelTv.setText(mMatch.getChannelName());
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
