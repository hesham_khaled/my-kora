package com.brighteyes.mykora.pref;

import android.content.SharedPreferences;

import java.util.Set;

public class SharedPreferencesHelperImpl implements SharedPreferencesHelper {

    private final SharedPreferences mSharedPreferences;

    public SharedPreferencesHelperImpl(SharedPreferences sharedPreferences) {
        mSharedPreferences = sharedPreferences;
    }

    @Override
    public String getString(String key) {
        return mSharedPreferences.getString(key, "");
    }

    @Override
    public void setString(String key, String value) {
        mSharedPreferences.edit().putString(key, value).apply();
    }

    @Override
    public int getInt(String key) {
        return mSharedPreferences.getInt(key, 0);
    }

    @Override
    public void setInt(String key, int value) {
        mSharedPreferences.edit().putInt(key, value).apply();
    }

    @Override
    public void setLong(String key, long value) {
        mSharedPreferences.edit().putLong(key, value).apply();
    }

    @Override
    public boolean getBoolean(String key) {
        return mSharedPreferences.getBoolean(key, false);
    }

    @Override
    public boolean getBoolean(String key, boolean defValue) {
        return mSharedPreferences.getBoolean(key, defValue);
    }

    @Override
    public void setBoolean(String key, boolean value) {
        mSharedPreferences.edit().putBoolean(key, value).apply();
    }

    @Override
    public void setStringSet(String key, Set<String> values) {
        mSharedPreferences.edit().putStringSet(key, values).apply();
    }

    @Override
    public long getLong(String matchId) {
        return mSharedPreferences.getLong(matchId, -1);
    }

    @Override
    public Set<String> getStringSet(String key) {
        return mSharedPreferences.getStringSet(key, null);
    }
}
