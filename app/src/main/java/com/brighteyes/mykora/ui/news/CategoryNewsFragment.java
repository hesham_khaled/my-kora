package com.brighteyes.mykora.ui.news;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.brighteyes.mykora.R;
import com.brighteyes.mykora.entities.Category;
import com.brighteyes.mykora.entities.News;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.helper.Utils;
import com.brighteyes.mykora.ui.base.BaseSupportFragment;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.brighteyes.mykora.helper.CommonConstants.FLAG_DELETE;
import static com.brighteyes.mykora.helper.CommonConstants.FLAG_INITIALIZE;
import static com.brighteyes.mykora.helper.CommonConstants.FLAG_INSERT;
import static com.brighteyes.mykora.helper.CommonConstants.FLAG_UPDATE;

public class CategoryNewsFragment extends BaseSupportFragment {

    private static final String ARG_CATEGORY = "category";
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.retry)
    TextView retry;
    @BindView(R.id.tvMessage)
    TextView tvMessage;
    @BindView(R.id.tryAgainLayout)
    ViewGroup tryAgainLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private NewsAdapter mNewsAdapter;
    private List<News> mNews = new ArrayList<>();
    private HashMap<String, News> mNewsHash = new HashMap<>();
    private Category category;

    public CategoryNewsFragment() {
        // Required empty public constructor
    }

    public static CategoryNewsFragment newInstance(Category category) {
        CategoryNewsFragment fragment = new CategoryNewsFragment();
        fragment.setTitle(category.getName());
        Bundle b = new Bundle();
        b.putParcelable(ARG_CATEGORY, Parcels.wrap(category));
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            category = Parcels.unwrap(getArguments().getParcelable(ARG_CATEGORY));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_recycler, container, false);
        ButterKnife.bind(this, rootView);
        if (category != null) {
            getCategoryNews();
        }
        return rootView;
    }

    private void getCategoryNews() {

        try {
            if (!Utils.getNetworkState(getActivity())) {
                showTryAgainLayout(R.string.connection_error);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        showProgressBar(true);
        hideTryAgainLayout();

        FirebaseDatabase.getInstance()
                .getReference()
                .child(CommonConstants.FB_NODE_NEWS)
                .orderByChild(CommonConstants.FB_NEWS_CATEGORY_ID).equalTo(category.getId())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // clear
                        mNews.clear();
                        mNewsHash.clear();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            News news = new News().parseNews(child);
                            mNews.add(news);
                            mNewsHash.put(news.getId(), news);
                        }
                        showProgressBar(false);
                        if (mNews.size() == 0) {
                            showTryAgainLayout(R.string.no_news);
                        }
                        updateNews(CommonConstants.FLAG_INITIALIZE, -1);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void updateNews(int transaction, int itemPosition) {
        switch (transaction) {
            case FLAG_INITIALIZE:
                initNewsRV();
                try {
                    addNewsChildValueListener();
                } catch (Exception ex) {
                    Log.d(CommonConstants.TAG, ex.getLocalizedMessage());
                }
                break;
            case FLAG_INSERT:
                mNewsAdapter.notifyItemInserted(mNews.size() - 1);
                break;
            case FLAG_UPDATE:
                mNewsAdapter.notifyItemChanged(itemPosition);
                break;
            case FLAG_DELETE:
                mNewsAdapter.notifyItemRemoved(itemPosition);
                break;
            default:
                break;
        }
    }

    private int getNewsPosition(String id) {
        int index = -1;
        for (News news : mNews) {
            if (!news.getId().equals("") && news.getId().equals(id)) {
                index = mNews.indexOf(news);
                break;
            }
        }
        return index;
    }

    private void initNewsRV() {
        recyclerView.setHasFixedSize(true);
        mNewsAdapter = new NewsAdapter(getActivity(), mNews, mNavigator);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mNewsAdapter);
    }

    private void addNewsChildValueListener() {

        FirebaseDatabase.getInstance().getReference()
                .child(CommonConstants.FB_NODE_NEWS)
                .orderByChild(CommonConstants.FB_NEWS_CATEGORY_ID).equalTo(category.getId())
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        News news = new News().parseNews(dataSnapshot);
                        if (!mNewsHash.containsKey(news.getId())) {
                            mNews.add(news);
                            mNewsHash.put(news.getId(), news);
                            hideTryAgainLayout();
                            updateNews(CommonConstants.FLAG_INSERT, mNews.size() - 1);
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        News news = new News().parseNews(dataSnapshot);

                        if (mNewsHash.containsKey(news.getId())) {
                            int position = getNewsPosition(news.getId());
                            mNews.set(position, news);
                            mNewsHash.put(news.getId(), news);
                            updateNews(CommonConstants.FLAG_UPDATE, position);
                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        News news = new News().parseNews(dataSnapshot);
                        if (mNewsHash.containsKey(news.getId())) {
                            int position = getNewsPosition(news.getId());
                            mNews.remove(position);
                            mNewsHash.remove(news.getId());
                            updateNews(CommonConstants.FLAG_DELETE, position);
                        }
                        if (mNews.size() == 0) {
                            showTryAgainLayout(R.string.no_videos);
                        }
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        showTryAgainLayout(databaseError.getMessage());
                    }
                });
    }

    @OnClick(R.id.retry)
    public void Onclick() {
        if (category != null) {
            getCategoryNews();
        }
    }

    private void showTryAgainLayout(int resId) {
        if (tryAgainLayout != null) {
            tryAgainLayout.setVisibility(View.VISIBLE);
            tvMessage.setTypeface(mTypeface);
            retry.setTypeface(mTypeface);
            tvMessage.setText(resId);
        }
    }

    private void showTryAgainLayout(String message) {
        if (tryAgainLayout != null) {
            tryAgainLayout.setVisibility(View.VISIBLE);
            tvMessage.setTypeface(mTypeface);
            retry.setTypeface(mTypeface);
            tvMessage.setText(message);
        }
    }

    private void hideTryAgainLayout() {
        if (tryAgainLayout != null) {
            tryAgainLayout.setVisibility(View.GONE);
        }
    }

    private void showProgressBar(boolean show) {
        if (progressBar != null) {
            if (show) {
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setIndeterminate(show);
            } else {
                progressBar.setVisibility(View.GONE);
            }
        }
    }

}