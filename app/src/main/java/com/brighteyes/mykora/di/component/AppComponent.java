package com.brighteyes.mykora.di.component;

import com.brighteyes.mykora.di.modules.AppModule;
import com.brighteyes.mykora.di.modules.StorageModule;
import com.brighteyes.mykora.ui.base.BaseFragment;
import com.brighteyes.mykora.ui.base.BaseSupportFragment;
import com.brighteyes.mykora.ui.base.component.BaseActivityComponent;
import com.brighteyes.mykora.ui.base.module.BaseActivityModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, StorageModule.class})
public interface AppComponent {

    void inject(BaseFragment baseFragment);

    void inject(BaseSupportFragment baseSupportFragment);

    BaseActivityComponent plus(BaseActivityModule module);
}
