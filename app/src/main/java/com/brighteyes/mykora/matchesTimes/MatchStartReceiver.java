package com.brighteyes.mykora.matchesTimes;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.brighteyes.mykora.entities.Match;
import com.brighteyes.mykora.entities.Notification;
import com.brighteyes.mykora.fcm.FirebaseHelper;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.helper.UIHelper;
import com.brighteyes.mykora.ui.settings.SettingsHelper;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.brighteyes.mykora.helper.CommonConstants.KEY_NOTIFICATION_DATA;
import static com.brighteyes.mykora.helper.CommonConstants.KEY_NOTIFICATION_TYPE;


/**
 * Created by Shanan on 16/08/2017.
 */

public class MatchStartReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent i) {
        Log.d(CommonConstants.TAG, "MatchStartReceiver onReceive");
        if (i != null) {
            String matchJSon = i.getStringExtra(CommonConstants.KEY_MATCH);

            try {
                // Update match status on server
                JSONObject dataObject = new JSONObject(matchJSon);
                Match match = new Match().parseMatch(dataObject);
                FirebaseHelper.updateMatchStatusOnServer(match.getId(), 1);

                // Create local notification
                Map<String, String> notificationMap = new HashMap<>();
                notificationMap.put(KEY_NOTIFICATION_TYPE, Notification.KEY_TYPE_MATCH_START);
                notificationMap.put(KEY_NOTIFICATION_DATA, matchJSon);
                Notification notification = Notification.getNotification(notificationMap);
                int matchNotificationSound = SettingsHelper.getInstance(context).getMatchNotificationSound();
                UIHelper.showNotification(context, notification, matchNotificationSound);
            } catch (Exception e) {
                Log.d(CommonConstants.TAG, e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void setAlarm(Context context, Match match) {

        SettingsHelper settingsHelper = SettingsHelper.getInstance(context);
        long matchStartTime = match.getMatchStartTime(settingsHelper);
        // check that no alarm is created for this match
        if (settingsHelper.getMatchStartTime(match.getId()) == -1) {
            Log.d(CommonConstants.TAG, "No Alarm was created for this match.");
            setAlarmTime(context, match, matchStartTime, settingsHelper);
        } else {
            Log.d(CommonConstants.TAG, "This match has an alarm already.");
            // Check that match time is not changed
            long savedTime = settingsHelper.getMatchStartTime(match.getId());
            if (savedTime != matchStartTime) {
                Log.d(CommonConstants.TAG, "Match has an alarm and Time Changed.");
                // Cancel old alarm
                int requestCode = settingsHelper.getMatchStartRequestCode(match.getId());
                cancelAlarm(context, requestCode);
                // set alarm time to the new time
                setAlarmTime(context, match, matchStartTime, settingsHelper);
            }
            Log.d(CommonConstants.TAG, "Match has an alarm and Time NOT changed.");
        }
    }

    public void cancelAlarm(Context context, int requestCode) {
        Log.d(CommonConstants.TAG, "Cancel Alarm.");
        Intent intent = new Intent(context, MatchStartReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, requestCode, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    private void setAlarmTime(Context context, Match match, long matchStartTime, SettingsHelper settingsHelper) {
        Log.d(CommonConstants.TAG, "Set Alarm Time.");
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, MatchStartReceiver.class);
        match.getMatchStatus();
        String matchJson = new Gson().toJson(match);
        intent.putExtra(CommonConstants.KEY_MATCH, matchJson);

        int requestCode = new Random().nextInt();
        PendingIntent pendingIntentAlarm = PendingIntent.getBroadcast(context, requestCode, intent, 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, matchStartTime, pendingIntentAlarm);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, matchStartTime, pendingIntentAlarm);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, matchStartTime, pendingIntentAlarm);
        }

        Log.d(CommonConstants.TAG, "Match start time: " + matchStartTime);
        Log.d(CommonConstants.TAG, "Request Code: " + requestCode);
        saveToSharedPreferences(settingsHelper, match.getId(), matchStartTime, requestCode);
    }

    private void saveToSharedPreferences(SettingsHelper settingsHelper, String matchId, long matchStartTime, int requestCode) {
        // save match id, start time, request code in sharedPreferences
        settingsHelper.setMatchStartTime(matchId, matchStartTime);
        settingsHelper.setMatchStartRequestCode(matchId, requestCode);
    }
}
