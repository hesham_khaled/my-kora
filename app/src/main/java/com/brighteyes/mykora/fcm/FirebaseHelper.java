package com.brighteyes.mykora.fcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.ui.settings.SettingsHelper;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Shanan on 29/03/2017.
 */

public class FirebaseHelper {

    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    public static void sendRegistrationToServer(final Context context, String refreshedToken) {
        final DatabaseReference fcmReference = FirebaseDatabase.getInstance()
                .getReference()
                .child(CommonConstants.FB_NODE_FCM_TOKENS);
        if (!SettingsHelper.getInstance(context).isFcmTokenSet()) {
            fcmReference.push().setValue(refreshedToken, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    if (databaseError != null) {
                        Log.d(TAG, "fcm token not registered");
                    } else {
                        Log.d(TAG, "fcm token registered");
                        setFCMTokenKey(context, fcmReference.getKey());
                    }
                }
            });
        } else {
            String key = SettingsHelper.getInstance(context).getFcmTokenKey();
            fcmReference.child(key).setValue(refreshedToken);
            Log.d(TAG, "fcm token registered");
            setFCMTokenKey(context, key);
        }

    }

    public static void updateMatchStatusOnServer(String matchId, long status) {
        final DatabaseReference fcmReference = FirebaseDatabase.getInstance()
                .getReference()
                .child(CommonConstants.FB_NODE_MATCH).child(matchId).child(CommonConstants.FB_MATCH_STATUS);
        fcmReference.setValue(status, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Log.d(CommonConstants.TAG, "fcm token not registered");
                }
            }
        });

    }


    private static void setFCMTokenKey(Context context, String remoteKey) {
        Log.d(TAG, "fcm remote key : " + remoteKey);
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(CommonConstants.KEY_IS_FCM_TOKEN_SET, true);
        editor.putString(CommonConstants.KEY_FCM_TOKEN, remoteKey);
        editor.apply();
    }
}
