package com.brighteyes.mykora.nav.Navigator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.brighteyes.mykora.entities.Match;
import com.brighteyes.mykora.entities.News;
import com.brighteyes.mykora.entities.Video;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.ui.custom.FullScreenVideo;
import com.brighteyes.mykora.ui.main.MainActivity;
import com.brighteyes.mykora.ui.matchInfo.MatchInfoActivity;
import com.brighteyes.mykora.ui.news.NewsDetailsActivity;
import com.brighteyes.mykora.ui.settings.NotificationSettingsActivity;
import com.brighteyes.mykora.ui.settings.TimeZoneActivity;
import com.brighteyes.mykora.ui.videos.VideoDetailsActivity;

import java.util.List;

/**
 * Created by Shanan on 01/02/2017.
 */
public class Navigator {

    private Context context;

    public void setContext(@NonNull Context context) {
        this.context = context;
    }

    public void navigateToMainActivity() {
        if (context != null) {
            Intent intent = MainActivity.buildIntent(context);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
        }
    }

    public void navigateToMatchInfoActivity(Match match) {
        if (context != null) {
            Intent intent = MatchInfoActivity.buildIntent(context, match);
            context.startActivity(intent);
        }
    }

    public void navigateToNewsDetailsActivity(News news) {
        if (context != null) {
            Intent intent = NewsDetailsActivity.buildIntent(context, news);
            context.startActivity(intent);
        }
    }

    public void navigateToVideoDetailsActivity(Video video) {
        if (context != null) {
            Intent intent = VideoDetailsActivity.buildIntent(context, video);
            context.startActivity(intent);
        }
    }

    public void navigateToTimeZonesActivity() {
        if (context != null) {
            Intent intent = TimeZoneActivity.buildIntent(context);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            context.startActivity(intent);
        }
    }

    public void navigateToNotificationActivity() {
        if (context != null) {
            Intent intent = NotificationSettingsActivity.buildIntent(context);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
            context.startActivity(intent);
        }
    }

    public void shareNewsItem(News newsItem) {
        if (context != null && newsItem != null) {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT,
                    newsItem.getTitle());
            shareIntent.putExtra(Intent.EXTRA_TEXT,
                    newsItem.getDetails());
            context.startActivity(shareIntent);
        }
    }

    public void shareVideo(Video video) {
        if (context != null && video != null) {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT,
                    video.getName());
            shareIntent.putExtra(Intent.EXTRA_TEXT,
                    video.getDescription());
            shareIntent.putExtra(Intent.EXTRA_TEXT,
                    video.getVideoUrl());
            context.startActivity(shareIntent);
        }
    }

    public void playVideo(Video video) {
        if (context != null && video != null) {

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(video.getVideoUrl()));
            intent.setDataAndType(Uri.parse(video.getVideoUrl()), "video/*");
            if (resolveIntent(intent)) {
                context.startActivity(intent);
            }
        }
    }

    private boolean resolveIntent(Intent intent) {
        PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> infos = packageManager.queryIntentActivities(intent, 0);
        if (infos.size() > 0) {
            return true;
        }
        return false;
    }

    public void navigateToFullScreen(Video video, int currentPosition, boolean isPlaying) {
        if (context != null) {
            Intent intent = FullScreenVideo.buildIntent(context, video, currentPosition, isPlaying);
            ((Activity) context).startActivityForResult(intent, CommonConstants.FULL_SCREEN_CODE);
        }
    }
}
