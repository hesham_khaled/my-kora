package com.brighteyes.mykora.pref;

import java.util.Set;

public interface SharedPreferencesHelper {

    String getString(String key);

    void setString(String key, String value);

    int getInt(String key);

    void setInt(String key, int value);

    void setLong(String key, long value);

    boolean getBoolean(String key);

    boolean getBoolean(String key, boolean defValue);

    void setBoolean(String key, boolean value);

    Set<String> getStringSet(String key);

    void setStringSet(String key, Set<String> value);

    long getLong(String matchId);
}
