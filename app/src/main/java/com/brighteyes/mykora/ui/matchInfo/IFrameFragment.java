package com.brighteyes.mykora.ui.matchInfo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.brighteyes.mykora.R;
import com.brighteyes.mykora.entities.MediaSource;
import com.brighteyes.mykora.ui.base.BaseSupportFragment;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;

public class IFrameFragment extends BaseSupportFragment {
    private static final String ARG_MEDIA_SOURCE = "media_source";
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.play)
    ImageView play;

    @BindView(R.id.customViewContainer)
    FrameLayout customViewContainer;
    private MediaSource mediaSource;
    private WebChromeClient.CustomViewCallback customViewCallback;
    private View mCustomView;
    private myWebChromeClient mWebChromeClient;
    private myWebViewClient mWebViewClient;

    public IFrameFragment() {
        // Required empty public constructor
    }

    public static IFrameFragment newInstance(MediaSource mediaSource) {
        IFrameFragment fragment = new IFrameFragment();
        fragment.setTitle(mediaSource.getName());
        Bundle args = new Bundle();
        args.putParcelable(ARG_MEDIA_SOURCE, Parcels.wrap(mediaSource));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mediaSource = Parcels.unwrap(getArguments().getParcelable(ARG_MEDIA_SOURCE));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_iframe, container, false);
        ButterKnife.bind(this, rootView);
        play.setVisibility(View.GONE);
        initWebView();
        return rootView;
    }

    private void initWebView() {
        webView.setVisibility(View.VISIBLE);
        customViewContainer.setVisibility(View.VISIBLE);

        mWebViewClient = new myWebViewClient();
        webView.setWebViewClient(mWebViewClient);

        mWebChromeClient = new myWebChromeClient();
        webView.setWebChromeClient(mWebChromeClient);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSaveFormData(true);
        webView.loadUrl(mediaSource.getBody());
    }

    public boolean inCustomView() {
        return (mCustomView != null);
    }

    public void hideCustomView() {
        mWebChromeClient.onHideCustomView();
    }

    @Override
    public void onPause() {
        super.onPause();
        webView.onPause();
    }

    //@Override
    //public void onResume() {
    //  super.onResume();
    //  webView.onResume();
    //  this.getView().setOnKeyListener(new View.OnKeyListener() {
    //    @Override
    //    public boolean onKey(View view, int i, KeyEvent keyEvent) {
    //      if (i == KeyEvent.KEYCODE_BACK) {
    //
    //        if (inCustomView()) {
    //          hideCustomView();
    //          return true;
    //        }
    //
    //        if ((mCustomView == null) && webView.canGoBack()) {
    //          webView.goBack();
    //          return true;
    //        }
    //      }
    //      return false;
    //    }
    //  });
    //}

    @Override
    public void onStop() {
        super.onStop();
        if (inCustomView()) {
            hideCustomView();
        }
    }

    class myWebChromeClient extends WebChromeClient {
        //private Bitmap mDefaultVideoPoster;
        private View mVideoProgressView;

        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
            onShowCustomView(view, callback);
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {

            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
            mCustomView = view;
            webView.setVisibility(View.GONE);
            customViewContainer.setVisibility(View.VISIBLE);
            customViewContainer.addView(view);
            customViewCallback = callback;
        }

        @Override
        public View getVideoLoadingProgressView() {

            if (mVideoProgressView == null) {
                LayoutInflater inflater = LayoutInflater.from(getActivity());
                mVideoProgressView = inflater.inflate(R.layout.video_progress, null);
            }
            return mVideoProgressView;
        }

        @Override
        public void onHideCustomView() {
            super.onHideCustomView();
            if (mCustomView == null) {
                return;
            }

            webView.setVisibility(View.VISIBLE);
            customViewContainer.setVisibility(View.GONE);

            // Hide the custom view.
            mCustomView.setVisibility(View.GONE);

            // Remove the custom view from its container.
            customViewContainer.removeView(mCustomView);
            customViewCallback.onCustomViewHidden();

            mCustomView = null;
        }
    }

    class myWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);
        }
    }
}