package com.brighteyes.mykora;

import com.brighteyes.mykora.entities.GoogleAdsId;

/**
 * Created by Shanan on 18/07/2017.
 */

public class Singleton {

    private static final Singleton ourInstance = new Singleton();
    private GoogleAdsId mGoogleAdsId;

    private Singleton() {
    }

    public static Singleton getInstance() {
        return ourInstance;
    }

    public GoogleAdsId getAdsIds() {
        return this.mGoogleAdsId;
    }

    public void setAdsIds(GoogleAdsId value) {
        mGoogleAdsId = value;
    }

}
