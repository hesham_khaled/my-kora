package com.brighteyes.mykora.di.modules;

import android.graphics.Typeface;
import android.support.annotation.NonNull;

import com.brighteyes.mykora.App;
import com.brighteyes.mykora.nav.Navigator.Navigator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @NonNull
    private final App app;

    public AppModule(@NonNull App app) {
        this.app = app;
    }

    @NonNull
    @Singleton
    @Provides
    public App providesApp() {
        return app;
    }

    @Singleton
    @Provides
    public Navigator providesNavigator() {
        return new Navigator();
    }

    @Provides
    @Singleton
    public Typeface provideTypeFace() {
        return Typeface.createFromAsset(app.getAssets(), "fonts/ae_Cortoba.ttf");
    }
}
