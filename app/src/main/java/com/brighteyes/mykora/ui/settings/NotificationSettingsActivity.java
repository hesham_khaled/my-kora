package com.brighteyes.mykora.ui.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.brighteyes.mykora.R;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.ui.base.BaseActivity;
import com.kyleduo.switchbutton.SwitchButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.brighteyes.mykora.helper.CommonConstants.FLAG_NOTIFICATION_SOUND;

public class NotificationSettingsActivity extends BaseActivity
        implements CompoundButton.OnCheckedChangeListener, RadioGroup.OnCheckedChangeListener {

    @BindView(R.id.activity_title)
    TextView activityTitle;

    @BindView(R.id.match_notifications)
    ViewGroup matchContainer;
    @BindView(R.id.match_notifications_switch)
    SwitchButton matchSwitchButton;
    @BindView(R.id.match_notifications_group)
    RadioGroup matchGroup;
    @BindView(R.id.match_label)
    TextView matchLabel;

    @BindView(R.id.news_notifications)
    ViewGroup newsContainer;
    @BindView(R.id.news_notifications_switch)
    SwitchButton newsSwitchButton;
    @BindView(R.id.news_notifications_group)
    RadioGroup newsGroup;
    @BindView(R.id.news_label)
    TextView newsLabel;

    @BindView(R.id.goals_notifications)
    ViewGroup goalsContainer;
    @BindView(R.id.goals_notifications_switch)
    SwitchButton goalsSwitchButton;
    @BindView(R.id.goals_notifications_group)
    RadioGroup goalsGroup;
    @BindView(R.id.goals_label)
    TextView goalsLabel;

    @BindView(R.id.videos_notifications)
    ViewGroup videosContainer;
    @BindView(R.id.videos_notifications_switch)
    SwitchButton videosSwitchButton;
    @BindView(R.id.videos_notifications_group)
    RadioGroup videosGroup;
    @BindView(R.id.videos_label)
    TextView videosLabel;

    @BindView(R.id.match_sound_radio)
    RadioButton matchSoundRadio;
    @BindView(R.id.match_mute_radio)
    RadioButton matchMuteRadio;
    @BindView(R.id.match_vibrate_radio)
    RadioButton matchVibrateRadio;
    @BindView(R.id.news_sound_radio)
    RadioButton newsSoundRadio;
    @BindView(R.id.news_mute_radio)
    RadioButton newsMuteRadio;
    @BindView(R.id.news_vibrate_radio)
    RadioButton newsVibrateRadio;
    @BindView(R.id.goals_sound_radio)
    RadioButton goalsSoundRadio;
    @BindView(R.id.goals_mute_radio)
    RadioButton goalsMuteRadio;
    @BindView(R.id.goals_vibrate_radio)
    RadioButton goalsVibrateRadio;
    @BindView(R.id.videos_sound_radio)
    RadioButton videosSoundRadio;
    @BindView(R.id.videos_mute_radio)
    RadioButton videosMuteRadio;
    @BindView(R.id.videos_vibrate_radio)
    RadioButton videosVibrateRadio;

    private int matchSelectedType = FLAG_NOTIFICATION_SOUND;
    private int newsSelectedType = FLAG_NOTIFICATION_SOUND;
    private int goalsSelectedType = FLAG_NOTIFICATION_SOUND;
    private int videosSelectedType = FLAG_NOTIFICATION_SOUND;

    public static Intent buildIntent(Context context) {
        return new Intent(context, NotificationSettingsActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_settings);
        ButterKnife.bind(this);

        activityTitle.setTypeface(mTypeface);
        activityTitle.setText(R.string.notifications_settings);

        matchLabel.setTypeface(mTypeface);
        newsLabel.setTypeface(mTypeface);
        goalsLabel.setTypeface(mTypeface);
        videosLabel.setTypeface(mTypeface);

        updateViewsFromSharedPreferences();
        addListeners();
    }

    private void updateViewsFromSharedPreferences() {
        matchSwitchButton.setChecked(mSettingsHelper.isMatchNotificationEnable());
        newsSwitchButton.setChecked(mSettingsHelper.isNewsNotificationEnable());
        goalsSwitchButton.setChecked(mSettingsHelper.isGoalNotificationEnable());
        videosSwitchButton.setChecked(mSettingsHelper.isVideoNotificationEnable());

        matchSelectedType = mSettingsHelper.getMatchNotificationSound();
        newsSelectedType = mSettingsHelper.getNewsNotificationSound();
        goalsSelectedType = mSettingsHelper.getGoalNotificationSound();
        videosSelectedType = mSettingsHelper.getVideoNotificationSound();

        matchGroup.check(getMatchRadioID(matchSelectedType));
        newsGroup.check(getNewsRadioID(newsSelectedType));
        goalsGroup.check(getGoalsRadioID(goalsSelectedType));
        videosGroup.check(getVideosRadioID(videosSelectedType));
    }

    private void addListeners() {
        matchSwitchButton.setOnCheckedChangeListener(this);
        newsSwitchButton.setOnCheckedChangeListener(this);
        goalsSwitchButton.setOnCheckedChangeListener(this);
        videosSwitchButton.setOnCheckedChangeListener(this);

        matchGroup.setOnCheckedChangeListener(this);
        newsGroup.setOnCheckedChangeListener(this);
        goalsGroup.setOnCheckedChangeListener(this);
        videosGroup.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        hideGroups();
        switch (compoundButton.getId()) {
            case R.id.match_notifications_switch:

                if (b) {
                    matchContainer.setVisibility(View.VISIBLE);
                }
                mSettingsHelper.setMatchNotificationEnable(b, matchSelectedType);

                break;

            case R.id.news_notifications_switch:

                if (b) {
                    newsContainer.setVisibility(View.VISIBLE);
                }
                mSettingsHelper.setNewsNotificationEnable(b, newsSelectedType);

                break;

            case R.id.goals_notifications_switch:

                if (b) {
                    goalsContainer.setVisibility(View.VISIBLE);
                }
                mSettingsHelper.setGoalNotificationEnable(b, goalsSelectedType);

                break;

            case R.id.videos_notifications_switch:

                if (b) {
                    videosContainer.setVisibility(View.VISIBLE);
                }
                mSettingsHelper.setVideoNotificationEnable(b, videosSelectedType);

                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {

        switch (radioGroup.getId()) {
            case R.id.match_notifications_group:
                saveMatchSettings(i);
                break;
            case R.id.news_notifications_group:
                saveNewsSettings(i);
                break;
            case R.id.goals_notifications_group:
                saveGoalsSettings(i);
                break;
            case R.id.videos_notifications_group:
                saveVideosSettings(i);
                break;
        }
    }

    private void hideGroups() {
        // hide all groups
        matchContainer.setVisibility(View.GONE);
        newsContainer.setVisibility(View.GONE);
        goalsContainer.setVisibility(View.GONE);
        videosContainer.setVisibility(View.GONE);
    }

    private void saveMatchSettings(int i) {
        switch (i) {
            case R.id.match_sound_radio:
                mSettingsHelper.setMatchNotificationEnable(true, CommonConstants.FLAG_NOTIFICATION_SOUND);
                break;
            case R.id.match_mute_radio:
                mSettingsHelper.setMatchNotificationEnable(true, CommonConstants.FLAG_NOTIFICATION_MUTE);
                break;
            case R.id.match_vibrate_radio:
                mSettingsHelper.setMatchNotificationEnable(true, CommonConstants.FLAG_NOTIFICATION_VIBRATE);
                break;
        }
    }

    private void saveNewsSettings(int i) {
        switch (i) {
            case R.id.news_sound_radio:
                mSettingsHelper.setNewsNotificationEnable(true, CommonConstants.FLAG_NOTIFICATION_SOUND);
                break;
            case R.id.news_mute_radio:
                mSettingsHelper.setNewsNotificationEnable(true, CommonConstants.FLAG_NOTIFICATION_MUTE);
                break;
            case R.id.news_vibrate_radio:
                mSettingsHelper.setNewsNotificationEnable(true, CommonConstants.FLAG_NOTIFICATION_VIBRATE);
                break;
        }
    }

    private void saveGoalsSettings(int i) {
        switch (i) {
            case R.id.goals_sound_radio:
                mSettingsHelper.setGoalNotificationEnable(true, CommonConstants.FLAG_NOTIFICATION_SOUND);
                break;
            case R.id.goals_mute_radio:
                mSettingsHelper.setGoalNotificationEnable(true, CommonConstants.FLAG_NOTIFICATION_MUTE);
                break;
            case R.id.goals_vibrate_radio:
                mSettingsHelper.setGoalNotificationEnable(true, CommonConstants.FLAG_NOTIFICATION_VIBRATE);
                break;
        }
    }

    private void saveVideosSettings(int i) {
        switch (i) {
            case R.id.videos_sound_radio:
                mSettingsHelper.setVideoNotificationEnable(true, CommonConstants.FLAG_NOTIFICATION_SOUND);
                break;
            case R.id.videos_mute_radio:
                mSettingsHelper.setVideoNotificationEnable(true, CommonConstants.FLAG_NOTIFICATION_MUTE);
                break;
            case R.id.videos_vibrate_radio:
                mSettingsHelper.setVideoNotificationEnable(true, CommonConstants.FLAG_NOTIFICATION_VIBRATE);
                break;
        }
    }

    private int getMatchRadioID(int matchSelectedSound) {
        switch (matchSelectedSound) {
            case CommonConstants.FLAG_NOTIFICATION_SOUND:
                return R.id.match_sound_radio;
            case CommonConstants.FLAG_NOTIFICATION_MUTE:
                return R.id.match_mute_radio;
            case CommonConstants.FLAG_NOTIFICATION_VIBRATE:
                return R.id.match_vibrate_radio;
            default:
                return R.id.match_sound_radio;

        }
    }

    private int getNewsRadioID(int newsSelectedSound) {
        switch (newsSelectedSound) {
            case CommonConstants.FLAG_NOTIFICATION_SOUND:
                return R.id.news_sound_radio;
            case CommonConstants.FLAG_NOTIFICATION_MUTE:
                return R.id.news_mute_radio;
            case CommonConstants.FLAG_NOTIFICATION_VIBRATE:
                return R.id.news_vibrate_radio;
            default:
                return R.id.news_sound_radio;
        }
    }

    private int getGoalsRadioID(int goalsSelectedSound) {
        switch (goalsSelectedSound) {
            case CommonConstants.FLAG_NOTIFICATION_SOUND:
                return R.id.goals_sound_radio;
            case CommonConstants.FLAG_NOTIFICATION_MUTE:
                return R.id.goals_mute_radio;
            case CommonConstants.FLAG_NOTIFICATION_VIBRATE:
                return R.id.goals_vibrate_radio;
            default:
                return R.id.goals_sound_radio;
        }
    }

    private int getVideosRadioID(int videosSelectedSound) {
        switch (videosSelectedSound) {
            case CommonConstants.FLAG_NOTIFICATION_SOUND:
                return R.id.videos_sound_radio;
            case CommonConstants.FLAG_NOTIFICATION_MUTE:
                return R.id.videos_mute_radio;
            case CommonConstants.FLAG_NOTIFICATION_VIBRATE:
                return R.id.videos_vibrate_radio;
            default:
                return R.id.videos_sound_radio;
        }
    }

    @OnClick(R.id.match_notifications_header)
    public void matchesClicked() {
        if (matchContainer.getVisibility() == View.VISIBLE) {
            hideGroups();
        } else {
            hideGroups();
            matchContainer.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.news_notifications_header)
    public void newsClicked() {
        if (newsContainer.getVisibility() == View.VISIBLE) {
            hideGroups();
        } else {
            hideGroups();
            newsContainer.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.goals_notifications_header)
    public void goalsClicked() {
        if (goalsContainer.getVisibility() == View.VISIBLE) {
            hideGroups();
        } else {
            hideGroups();
            goalsContainer.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.videos_notifications_header)
    public void videosClicked() {
        if (videosContainer.getVisibility() == View.VISIBLE) {
            hideGroups();
        } else {
            hideGroups();
            videosContainer.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.ic_time)
    public void timeIconClick() {
        mNavigator.navigateToTimeZonesActivity();
    }

    @OnClick(R.id.ic_home_action)
    public void logoClick() {
        mNavigator.navigateToMainActivity();
    }
}