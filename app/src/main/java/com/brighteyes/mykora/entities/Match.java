package com.brighteyes.mykora.entities;

import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.helper.Utils;
import com.brighteyes.mykora.ui.settings.SettingsHelper;
import com.google.firebase.database.DataSnapshot;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

/**
 * Created by Shanan on 11/02/2017.
 */
@Parcel
public class Match {
    @SerializedName(CommonConstants.FB_MATCH_ID)
    String id;
    @SerializedName(CommonConstants.FB_MATCH_CHAMP_LOGO_URL)
    String champLogoURL;
    @SerializedName(CommonConstants.FB_MATCH_CHAMP_NAME)
    String champName;
    @SerializedName(CommonConstants.FB_MATCH_CHAMP_ID)
    String champID;
    @SerializedName(CommonConstants.FB_MATCH_CHANNEL_ID)
    String channelID;
    @SerializedName(CommonConstants.FB_MATCH_CHANNEL_NAME)
    String channelName;
    @SerializedName(CommonConstants.FB_MATCH_CLUB1_ID)
    String leftID;
    @SerializedName(CommonConstants.FB_MATCH_CLUB1_NAME)
    String leftName;
    @SerializedName(CommonConstants.FB_MATCH_CLUB1_LOGO_URL)
    String leftLogo;
    @SerializedName(CommonConstants.FB_MATCH_CLUB1_SCORE)
    long leftScore;
    @SerializedName(CommonConstants.FB_MATCH_CLUB2_ID)
    String rightID;
    @SerializedName(CommonConstants.FB_MATCH_CLUB2_NAME)
    String rightName;
    @SerializedName(CommonConstants.FB_MATCH_CLUB2_LOGO_URL)
    String rightLogo;
    @SerializedName(CommonConstants.FB_MATCH_CLUB2_SCORE)
    long rightScore;
    @SerializedName(CommonConstants.FB_MATCH_COMMENTATOR_ID)
    String commentatorID;
    @SerializedName(CommonConstants.FB_MATCH_COMMENTATOR_NAME)
    String commentatorName;
    @SerializedName(CommonConstants.FB_MATCH_DESC)
    String description;
    @SerializedName(CommonConstants.FB_MATCH_STATUS)
    long matchStatus;
    @SerializedName(CommonConstants.FB_MATCH_REFEREE_ID)
    String refereeID;
    @SerializedName(CommonConstants.FB_MATCH_REFEREE_NAME)
    String refereeName;
    @SerializedName(CommonConstants.FB_MATCH_STADIUM_ID)
    String stadID;
    @SerializedName(CommonConstants.FB_MATCH_STADIUM_NAME)
    String stadName;
    @SerializedName(CommonConstants.FB_MATCH_TAGS)
    String tags;
    @SerializedName(CommonConstants.FB_MATCH_TIME)
    long matchStartTime;

    public Match() {
    }

    @ParcelConstructor
    public Match(String id, String champLogoURL, String champName, String channelID, String channelName, String leftID, String leftName, String leftLogo, long leftScore, String rightID, String rightName, String rightLogo, long rightScore, String commentatorID, String commentatorName, String description, long matchStatus, String refereeID, String refereeName, String stadID, String stadName, String tags, long matchStartTime, String champID) {
        this.id = id;
        this.champLogoURL = champLogoURL;
        this.champName = champName;
        this.channelID = channelID;
        this.channelName = channelName;
        this.leftID = leftID;
        this.leftName = leftName;
        this.leftLogo = leftLogo;
        this.leftScore = leftScore;
        this.rightID = rightID;
        this.rightName = rightName;
        this.rightLogo = rightLogo;
        this.rightScore = rightScore;
        this.commentatorID = commentatorID;
        this.commentatorName = commentatorName;
        this.description = description;
        this.matchStatus = matchStatus;
        this.refereeID = refereeID;
        this.refereeName = refereeName;
        this.stadID = stadID;
        this.stadName = stadName;
        this.tags = tags;
        this.matchStartTime = matchStartTime;
        this.champID = champID;
    }

    public Match parseMatch(DataSnapshot dataSnapshot) {
        this.id = dataSnapshot.getKey();

        this.champLogoURL = (String) dataSnapshot.child(CommonConstants.FB_MATCH_CHAMP_LOGO_URL)
                .getValue();
        this.champName = (String) dataSnapshot.child(CommonConstants.FB_MATCH_CHAMP_NAME).getValue();
        this.champID = (String) dataSnapshot.child(CommonConstants.FB_MATCH_CHAMP_ID).getValue();
        this.channelID = (String) dataSnapshot.child(CommonConstants.FB_MATCH_CHANNEL_ID).getValue();
        this.channelName = (String) dataSnapshot.child(CommonConstants.FB_MATCH_CHANNEL_NAME)
                .getValue();
        this.leftID = (String) dataSnapshot.child(CommonConstants.FB_MATCH_CLUB1_ID).getValue();
        this.leftName = (String) dataSnapshot.child(CommonConstants.FB_MATCH_CLUB1_NAME).getValue();
        this.leftLogo = (String) dataSnapshot.child(CommonConstants.FB_MATCH_CLUB1_LOGO_URL).getValue();
        this.leftScore = (Long) dataSnapshot.child(CommonConstants.FB_MATCH_CLUB1_SCORE).getValue();
        this.rightID = (String) dataSnapshot.child(CommonConstants.FB_MATCH_CLUB2_ID).getValue();
        this.rightName = (String) dataSnapshot.child(CommonConstants.FB_MATCH_CLUB2_NAME).getValue();
        this.rightLogo = (String) dataSnapshot.child(CommonConstants.FB_MATCH_CLUB2_LOGO_URL)
                .getValue();
        this.rightScore = (Long) dataSnapshot.child(CommonConstants.FB_MATCH_CLUB2_SCORE).getValue();
        this.commentatorID = (String) dataSnapshot.child(CommonConstants.FB_MATCH_COMMENTATOR_ID)
                .getValue();
        this.commentatorName = (String) dataSnapshot.child(CommonConstants.FB_MATCH_COMMENTATOR_NAME)
                .getValue();
        this.description = (String) dataSnapshot.child(CommonConstants.FB_MATCH_DESC).getValue();
        this.matchStatus = (Long) dataSnapshot.child(CommonConstants.FB_MATCH_STATUS).getValue();
        this.refereeID = (String) dataSnapshot.child(CommonConstants.FB_MATCH_REFEREE_ID).getValue();
        this.refereeName = (String) dataSnapshot.child(CommonConstants.FB_MATCH_REFEREE_NAME)
                .getValue();
        this.stadID = (String) dataSnapshot.child(CommonConstants.FB_MATCH_STADIUM_ID).getValue();
        this.stadName = (String) dataSnapshot.child(CommonConstants.FB_MATCH_STADIUM_NAME).getValue();
        this.tags = (String) dataSnapshot.child(CommonConstants.FB_MATCH_TAGS).getValue();
        this.matchStartTime = (Long) dataSnapshot.child(CommonConstants.FB_MATCH_TIME).getValue();

        return this;
    }

    public String getLeftName() {
        return leftName;
    }

    public String getRightName() {
        return rightName;
    }

    public String getLeftLogo() {
        return leftLogo;
    }

    public String getRightLogo() {
        return rightLogo;
    }

    public long getLeftScore() {
        return leftScore;
    }

    public long getRightScore() {
        return rightScore;
    }

    public int getMatchStatus() {
        return (int) matchStatus;
    }

    public void setMatchStatus(long matchStatus) {
        this.matchStatus = matchStatus;
    }

    public String getMatchStatusString() {
        String status = "";
        switch ((int) matchStatus) {
            case 0:
                status = "لم تبدأ";
                break;
            case 1:
                status = "مباشر";
                break;
            case 2:
                status = "انتهت";
                break;
        }
        return status;
    }

    public long getMatchStartTime(SettingsHelper settingsHelper) {
        return Utils.applyTimeZone(matchStartTime, settingsHelper);
    }

    public String getChampLogoURL() {
        return champLogoURL;
    }

    public String getChampName() {
        return champName;
    }

    public String getChannelID() {
        return channelID;
    }

    public String getChannelName() {
        return channelName;
    }

    public String getLeftID() {
        return leftID;
    }

    public String getRightID() {
        return rightID;
    }

    public String getCommentatorID() {
        return commentatorID;
    }

    public String getCommentatorName() {
        return commentatorName;
    }

    public String getDescription() {
        return description;
    }

    public String getRefereeID() {
        return refereeID;
    }

    public String getRefereeName() {
        return refereeName;
    }

    public String getStadID() {
        return stadID;
    }

    public String getStadName() {
        return stadName;
    }

    public String getTags() {
        return tags;
    }

    public String getId() {
        return id;
    }

    public String getChampID() {
        return champID;
    }

    public int getDay(SettingsHelper settingsHelper) {
        return Utils.getMatchDay(matchStartTime, settingsHelper.getTimezone());
    }

    public String getTime(SettingsHelper settingsHelper) {

        return Utils.getTime(matchStartTime, settingsHelper.getTimezone());
    }

    public Match parseMatch(JSONObject matchObject) throws JSONException {
        Match match = new Match();

        match.id = matchObject.getString(CommonConstants.FB_MATCH_ID);
        match.champLogoURL = matchObject.getString(CommonConstants.FB_MATCH_CHAMP_LOGO_URL);
        match.champName = matchObject.getString(CommonConstants.FB_MATCH_CHAMP_NAME);
        match.champID = matchObject.getString(CommonConstants.FB_MATCH_CHAMP_ID);
        match.channelID = matchObject.getString(CommonConstants.FB_MATCH_CHANNEL_ID);
        match.channelName = matchObject.getString(CommonConstants.FB_MATCH_CHANNEL_NAME);
        match.leftID = matchObject.getString(CommonConstants.FB_MATCH_CLUB1_ID);
        match.leftName = matchObject.getString(CommonConstants.FB_MATCH_CLUB1_NAME);
        match.leftLogo = matchObject.getString(CommonConstants.FB_MATCH_CLUB1_LOGO_URL);
        match.leftScore = matchObject.getLong(CommonConstants.FB_MATCH_CLUB1_SCORE);
        match.rightID = matchObject.getString(CommonConstants.FB_MATCH_CLUB2_ID);
        match.rightName = matchObject.getString(CommonConstants.FB_MATCH_CLUB2_NAME);
        match.rightLogo = matchObject.getString(CommonConstants.FB_MATCH_CLUB2_LOGO_URL);
        match.rightScore = matchObject.getLong(CommonConstants.FB_MATCH_CLUB2_SCORE);
        match.commentatorID = matchObject.getString(CommonConstants.FB_MATCH_COMMENTATOR_ID);
        match.commentatorName = matchObject.getString(CommonConstants.FB_MATCH_COMMENTATOR_NAME);
        match.description = matchObject.getString(CommonConstants.FB_MATCH_DESC);
        match.matchStatus = matchObject.getLong(CommonConstants.FB_MATCH_STATUS);
        match.refereeID = matchObject.getString(CommonConstants.FB_MATCH_REFEREE_ID);
        match.refereeName = matchObject.getString(CommonConstants.FB_MATCH_REFEREE_NAME);
        match.stadID = matchObject.getString(CommonConstants.FB_MATCH_STADIUM_ID);
        match.stadName = matchObject.getString(CommonConstants.FB_MATCH_STADIUM_NAME);
        match.tags = matchObject.getString(CommonConstants.FB_MATCH_TAGS);
        match.matchStartTime = matchObject.getLong(CommonConstants.FB_MATCH_TIME);

        return match;
    }
}