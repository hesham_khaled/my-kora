package com.brighteyes.mykora.helper;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.brighteyes.mykora.R;
import com.brighteyes.mykora.entities.Match;
import com.brighteyes.mykora.entities.News;
import com.brighteyes.mykora.entities.Notification;
import com.brighteyes.mykora.entities.Video;
import com.brighteyes.mykora.ui.main.MainActivity;
import com.brighteyes.mykora.ui.matchInfo.MatchInfoActivity;
import com.brighteyes.mykora.ui.news.NewsDetailsActivity;
import com.brighteyes.mykora.ui.settings.SettingsHelper;
import com.brighteyes.mykora.ui.videos.VideoDetailsActivity;

/**
 * Created by Shanan on 18/02/2017.
 */

public class UIHelper {
    public static void showSnackBar(View view, int resId) {

        if (view != null) {
            Snackbar.make(view, resId, Snackbar.LENGTH_LONG).show();
        }
    }

    public static void setSmallScript(TextView textView, String text, String subScript,
                                      int subScriptColorRes) {

        int subScriptColor = ContextCompat.getColor(textView.getContext(), subScriptColorRes);
        String сolorString = String.format("%X", subScriptColor).substring(2); // !!strip alpha value!!
        textView.setText(Html.fromHtml(
                String.format(text + " <small><font color=\"#%s\">" + subScript + "</font></small>",
                        сolorString)), TextView.BufferType.SPANNABLE);
    }

    public static DisplayMetrics getScreenDimensions(Activity activity) {
        /**
         * Get screen dimensions
         */
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        return displayMetrics;
    }

    public static int convertToPx(float dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                context.getResources().getDisplayMetrics());
    }

    public static void setPaddingDp(Context context, View view, float left, float top, float right, float bottom) {
        view.setPadding(
                convertToPx(left, context),
                convertToPx(top, context),
                convertToPx(right, context),
                convertToPx(bottom, context));
    }

    private static int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT
                >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.logo_transparent : R.drawable.logo;
    }

    private static Bitmap getNotificationLargeIcon() {
        try {
            Bitmap bm = BitmapFactory.decodeResource(null, R.id.image);
            return bm;
        } catch (Exception ex) {
            return null;
        }
    }

    public static String formatHtml(String html) {

        html = html.replaceAll("<img.+/(img)*>", "");

        html = Html.fromHtml(html).toString();
        return html;
    }

    public static String getContentImage(String content) {

        int start = content.indexOf("src=\"") + 5;
        int end = content.indexOf("\"", start);

        return content.substring(start, end);
    }

    public static void showSnackbar(ViewGroup container, int resId) {
        if (container != null) {
            Snackbar.make(container, resId, Snackbar.LENGTH_LONG).show();
        }
    }

    public static void positionToast(Toast toast, View view, Window window, int offsetX,
                                     int offsetY) {
        // toasts are positioned relatively to decor view, views relatively to their parents, we have to gather additional data to have a common coordinate system
        Rect rect = new Rect();
        window.getDecorView().getWindowVisibleDisplayFrame(rect);
        // covert anchor view absolute position to a position which is relative to decor view
        int[] viewLocation = new int[2];
        view.getLocationInWindow(viewLocation);
        int viewLeft = viewLocation[0] - rect.left;
        int viewTop = viewLocation[1] - rect.top;

        // measure toast to center it relatively to the anchor view
        DisplayMetrics metrics = new DisplayMetrics();
        window.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(metrics.widthPixels,
                View.MeasureSpec.UNSPECIFIED);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(metrics.heightPixels,
                View.MeasureSpec.UNSPECIFIED);
        toast.getView().measure(widthMeasureSpec, heightMeasureSpec);
        int toastWidth = toast.getView().getMeasuredWidth();

        // compute toast offsets
        int toastX = viewLeft + (view.getWidth() - toastWidth) / 2 + offsetX;
        int toastY = viewTop + view.getHeight() + offsetY;

        toast.setGravity(Gravity.LEFT | Gravity.TOP, toastX, toastY);
    }

    // Creates notification based on title and body received
    public static void showNotification(Context context, Notification notification, int soundSetting) {

        final int MESSAGE_NOTIFICATION_ID = notification.getId();
        Log.d(CommonConstants.TAG, notification.getType());
        // define sound URI, the sound to be played when there's a notification
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        PendingIntent pIntent;
        Intent intent;

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        String title;
        String summary = "";
        String description = "";

        if (notification.isMatchNotification()) {
            Match match = (Match) notification.getDataObject();
            title = "مباراة " + match.getRightName() + " و" + match.getLeftName();
            summary = match.getChampName();
            SettingsHelper settingsHelper = SettingsHelper.getInstance(context);
            description = "تبدأ في " + match.getTime(settingsHelper);

            intent = MatchInfoActivity.buildIntent(context, match);
            stackBuilder.addParentStack(MatchInfoActivity.class);
        } else if (notification.isGoalNotification()) {
            Match match = (Match) notification.getDataObject();
            title = "جووووووووووووول";
            summary = match.getChampName();

            intent = MatchInfoActivity.buildIntent(context, (Match) notification.getDataObject());
            stackBuilder.addParentStack(MatchInfoActivity.class);
        } else if (notification.isNewsNotification()) {
            News news = (News) notification.getDataObject();
            title = news.getTitle();
            summary = news.getCatName();

            intent = NewsDetailsActivity.buildIntent(context, news);
            stackBuilder.addParentStack(NewsDetailsActivity.class);
        } else if (notification.isVideoNotification()) {
            Video video = (Video) notification.getDataObject();
            title = video.getName();
            summary = video.getCategoryName();

            intent = VideoDetailsActivity.buildIntent(context, video);
            stackBuilder.addParentStack(VideoDetailsActivity.class);
        } else {
            title = context.getString(R.string.app_name);

            intent = MainActivity.buildIntent(context);
            stackBuilder.addParentStack(MainActivity.class);
        }
        // Sets the Activity to start in a new, empty task
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        // Adds the Intent to the top of the stack
        stackBuilder.addNextIntent(intent);

        // Gets a PendingIntent containing the entire back stack
        pIntent = stackBuilder.getPendingIntent(notification.getId(), PendingIntent.FLAG_UPDATE_CURRENT);

        // Create the style object with BigTextStyle subclass.
        NotificationCompat.BigTextStyle notiStyle = new NotificationCompat.BigTextStyle();
        notiStyle.setBigContentTitle(title);
        notiStyle.bigText(description);
        notiStyle.setSummaryText(summary);

        // Now we can attach this to the notification using setContentIntent
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context).setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(
                                BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                        .setContentTitle(title)
                        .setContentIntent(pIntent)
                        .setAutoCancel(true)
                        .setStyle(notiStyle);
        switch (soundSetting) {
            case CommonConstants.FLAG_NOTIFICATION_SOUND:
                mBuilder.setSound(soundUri);
                break;
            case CommonConstants.FLAG_NOTIFICATION_VIBRATE:
                mBuilder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
            case CommonConstants.FLAG_NOTIFICATION_MUTE:
                mBuilder.setLights(Color.BLUE, 3000, 3000);
                break;
        }

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(MESSAGE_NOTIFICATION_ID, mBuilder.build());
    }

}
