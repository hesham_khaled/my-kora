package com.brighteyes.mykora.ui.news;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brighteyes.mykora.App;
import com.brighteyes.mykora.R;
import com.brighteyes.mykora.entities.News;
import com.brighteyes.mykora.nav.Navigator.Navigator;
import com.makeramen.roundedimageview.RoundedImageView;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shanan on 30/01/2017.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    private Activity mContext;
    private List<News> newsList;
    private Typeface typeface;
    private Navigator navigator;

    public NewsAdapter(Activity context, List<News> newsList, Navigator navigator) {
        mContext = context;
        this.newsList = newsList;
        typeface = App.defaultFont;
        this.navigator = navigator;
    }

    @Override
    public int getItemViewType(int position) {
        return position % 2;
    }

    @Override
    public NewsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case 0:
                view = LayoutInflater.from(mContext).inflate(R.layout.news_right_item, parent, false);
                break;
            case 1:
                view = LayoutInflater.from(mContext).inflate(R.layout.news_left_item, parent, false);
                break;
            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.news_right_item, parent, false);
        }

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final News newsItem = newsList.get(position);

        if (newsItem != null) {
            bindData(holder, newsItem);
        }
    }

    private void bindData(final ViewHolder holder, final News newsItem) {

        Transformation transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(30)
                .oval(false)
                .build();

        Picasso.with(mContext)
                .load(newsItem.getPhoto_url())
                .fit()
                .placeholder(R.drawable.logo_transparent)
                .transform(transformation)
                .into(holder.image);

        holder.title.setText(newsItem.getTitle());
        holder.subTitle.setText(newsItem.getCatName());

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigator.navigateToNewsDetailsActivity(newsItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.news_item_container)
        ViewGroup container;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.news_img)
        RoundedImageView image;
        @BindView(R.id.sub)
        TextView subTitle;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            title.setTypeface(typeface);
            subTitle.setTypeface(typeface);
            image.setBackgroundColor(ContextCompat.getColor(mContext, R.color.tabSelectedBG));
        }
    }
}