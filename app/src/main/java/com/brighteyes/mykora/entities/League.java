package com.brighteyes.mykora.entities;

import com.brighteyes.mykora.helper.CommonConstants;
import com.google.firebase.database.DataSnapshot;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shanan on 11/02/2017.
 */

@Parcel
public class League {
    String id;
    String name;
    String logoURL;
    List<Match> matches = new ArrayList<>();

    public League() {

    }


    public League parseLeague(DataSnapshot dataSnapshot) {
        this.id = dataSnapshot.getKey();
        this.name = (String) dataSnapshot.child(CommonConstants.FB_CHAMP_NAME).getValue();
        this.logoURL = (String) dataSnapshot.child(CommonConstants.FB_CHAMP_LOGO_URL).getValue();
        return this;
    }

    public String getName() {
        return name;
    }

    public List<Match> getMatches() {
        return matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }

    public String getId() {
        return id;
    }

    public String getLogoURL() {
        return logoURL;
    }
}
