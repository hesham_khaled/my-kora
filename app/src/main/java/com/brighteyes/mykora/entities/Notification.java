package com.brighteyes.mykora.entities;

import android.support.annotation.Nullable;
import android.util.Log;

import com.brighteyes.mykora.helper.CommonConstants;

import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;
import java.util.Map;

import static com.brighteyes.mykora.helper.CommonConstants.KEY_NOTIFICATION_DATA;
import static com.brighteyes.mykora.helper.CommonConstants.KEY_NOTIFICATION_TYPE;

public class Notification {

    public final static String KEY_TYPE_MATCH_START = "match_status_start";
    public final static String KEY_TYPE_MATCH_ENDED = "match_status_ended";
    private final static String KEY_TYPE_MATCH = "new_match";
    private final static String KEY_TYPE_NEWS = "new_news";
    private final static String KEY_TYPE_VIDEO = "new_video";
    private final static String KEY_TYPE_GOAL = "match_scores_changed";
    private int id;
    private String type;
    private Object data;
    private String notificationTime;

    Notification() {
    }

    @Nullable
    public static Notification getNotification(Map<String, String> notificationMap) {

        if (notificationMap.get(KEY_NOTIFICATION_TYPE) == null) {
            return null;
        }

        try {
            //Create new notification
            Notification notification = new Notification();

            notification.notificationTime = DateFormat.getDateTimeInstance().format(new Date());

            notification.id = (int) System.currentTimeMillis();
            //Set notification type
            notification.type = notificationMap.get(KEY_NOTIFICATION_TYPE);
            String data = notificationMap.get(KEY_NOTIFICATION_DATA);

            JSONObject dataObject = new JSONObject(data);
            if (notification.isMatchNotification()) {
                Match match = new Match().parseMatch(dataObject);
                if (notification.type.equalsIgnoreCase(KEY_TYPE_MATCH_START)) {
                    match.setMatchStatus(1);
                } else if (notification.type.equalsIgnoreCase(KEY_TYPE_MATCH_ENDED)) {
                    match.setMatchStatus(2);
                }
                notification.data = match;
                return notification;
            }
            if (notification.isVideoNotification()) {
                Video video = new Video().parseVideo(dataObject);
                notification.data = video;
                return notification;
            }
            if (notification.isNewsNotification()) {
                News news = new News().parseNews(dataObject);
                notification.data = news;
                return notification;
            } else return null;

        } catch (Exception e) {
            Log.e(CommonConstants.TAG, "notification exception: " + e.toString());
            e.printStackTrace();
            return null;
        }
    }

    public boolean isMatchNotification() {
        return (type.equalsIgnoreCase(KEY_TYPE_MATCH)
                || type.equalsIgnoreCase(KEY_TYPE_MATCH_START)
                || type.equalsIgnoreCase(KEY_TYPE_MATCH_ENDED));
    }

    public boolean isGoalNotification() {
        return type.equalsIgnoreCase(KEY_TYPE_GOAL);
    }

    public boolean isNewsNotification() {
        return type.equalsIgnoreCase(KEY_TYPE_NEWS);
    }

    public boolean isVideoNotification() {
        return type.equalsIgnoreCase(KEY_TYPE_VIDEO);
    }

    public String getNotificationTime() {
        return notificationTime;
    }

    public String getType() {
        return type;
    }

    public Object getDataObject() {
        return data;
    }

    public int getId() {
        return id;
    }

}
