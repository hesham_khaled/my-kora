package com.brighteyes.mykora.ui.videos;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.brighteyes.mykora.R;
import com.brighteyes.mykora.entities.Category;
import com.brighteyes.mykora.entities.Video;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.helper.Utils;
import com.brighteyes.mykora.ui.base.BaseSupportFragment;
import com.brighteyes.mykora.ui.base.PagerAdapter;
import com.brighteyes.mykora.ui.custom.NonSwipeableViewPager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VideosFragment extends BaseSupportFragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    @BindView(R.id.recommended_layout)
    ViewGroup recommendedLayout;
    @BindView(R.id.recommendedTV)
    TextView recommendedTV;
    @BindView(R.id.recommended_rv)
    RecyclerView recommendedRV;
    @BindView(R.id.categories_viewpager)
    NonSwipeableViewPager mainViewPager;
    @BindView(R.id.content_tabs)
    TabLayout tabLayout;
    @BindView(R.id.retry)
    TextView retry;
    @BindView(R.id.tvMessage)
    TextView tvMessage;
    @BindView(R.id.tryAgainLayout)
    ViewGroup tryAgainLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    List<BaseSupportFragment> fragments = new ArrayList<>();
    ArrayList<String> ids = new ArrayList<>();
    private PagerAdapter mPagerAdapter;
    private VideosAdapter mRecommendedAdapter;
    private List<Video> recommendedVideos = new ArrayList<>();
    private boolean mIsRtl;

    public VideosFragment() {
        // Required empty public constructor
    }

    public static VideosFragment newInstance(String fragName) {
        VideosFragment fragment = new VideosFragment();
        fragment.setTitle(fragName);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIsRtl = mSettingsHelper.getLang().equalsIgnoreCase(CommonConstants.ARABIC_LOCALE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_videos, container, false);

        ButterKnife.bind(this, rootView);

        recommendedTV.setTypeface(mTypeface);

        addDividers(tabLayout);

        getCategories();

        getRecommendedVideos();

        return rootView;
    }

    private void getCategories() {

        try {
            if (!Utils.getNetworkState(getActivity())) {
                showTryAgainLayout(R.string.connection_error);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        showProgressBar(true);

        final boolean[] isLoaded = {false};

        FirebaseDatabase.getInstance()
                .getReference()
                .child(CommonConstants.FB_NODE_VIDEO_CATEGORY)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        fragments.clear();
                        ids.clear();
                        isLoaded[0] = true;
                        showProgressBar(false);
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            Category category = new Category().parseCategory(child);
                            if (ids.indexOf(category.getId()) == -1) {
                                fragments.add(CategoryVideosFragment.newInstance(category));
                                ids.add(category.getId());
                            }
                        }
                        fragments.add(LatestVideosFragment.newInstance());
                        if (mIsRtl) {
                            Collections.reverse(fragments);
                        }
                        updateTabs(CommonConstants.FLAG_INITIALIZE);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isLoaded[0]) {
                    showTryAgainLayout(R.string.no_videos);
                    showProgressBar(false);
                }
            }
        }, 5000);
    }

    private void updateTabs(int transaction) {
        hideTryAgainLayout();
        switch (transaction) {
            case CommonConstants.FLAG_INITIALIZE:
                initTabs();
                break;
            case CommonConstants.FLAG_INSERT:
                mPagerAdapter.notifyDataSetChanged();
                break;
            case CommonConstants.FLAG_UPDATE:
                mPagerAdapter.notifyDataSetChanged();
                break;
            case CommonConstants.FLAG_DELETE:
                mPagerAdapter.notifyDataSetChanged();
                break;
        }
    }

    private void initTabs() {
        mPagerAdapter = new PagerAdapter(getChildFragmentManager(), fragments);
        mainViewPager.setAdapter(mPagerAdapter);
        tabLayout.setupWithViewPager(mainViewPager);
        mainViewPager.setCurrentItem(mainViewPager.getAdapter().getCount() - 1, false);
    }

    private void getRecommendedVideos() {

        FirebaseDatabase.getInstance()
                .getReference()
                .child(CommonConstants.FB_NODE_VIDEO)
                .orderByChild(CommonConstants.FB_VID_IS_IMPORTANT).equalTo(true)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        recommendedVideos.clear();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            recommendedVideos.add(new Video().parseVideo(child));
                        }
                        if (recommendedVideos.size() > 0) {
                            recommendedLayout.setVisibility(View.VISIBLE);
                            updateRecommendedVideos(CommonConstants.FLAG_INITIALIZE, -1);
                        } else {
                            recommendedLayout.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void updateRecommendedVideos(int transaction, int itemPosition) {
        switch (transaction) {
            case CommonConstants.FLAG_INITIALIZE:
                initRecommended();
                break;
            case CommonConstants.FLAG_INSERT:
                mRecommendedAdapter.notifyItemInserted(recommendedVideos.size() - 1);
                break;
            case CommonConstants.FLAG_UPDATE:
                mRecommendedAdapter.notifyItemChanged(itemPosition);
                break;
            case CommonConstants.FLAG_DELETE:
                mRecommendedAdapter.notifyItemRemoved(itemPosition);
                break;
        }
    }

    private void initRecommended() {
        recommendedRV.setHasFixedSize(true);
        mRecommendedAdapter = new VideosAdapter(getActivity(), recommendedVideos, true, mNavigator);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);
        recommendedRV.setLayoutManager(layoutManager);
        recommendedRV.setAdapter(mRecommendedAdapter);
    }

    private int getVideoPosition(String id) {
        int index = -1;
        for (Video video : recommendedVideos) {
            if (!video.getId().equals("") && video.getId().equals(id)) {
                index = recommendedVideos.indexOf(video);
                break;
            }
        }
        return index;
    }

    @OnClick(R.id.retry)
    public void Onclick() {
        hideTryAgainLayout();
        showProgressBar(true);
        getCategories();
        getRecommendedVideos();
    }

    private void showTryAgainLayout(int resId) {
        if (tryAgainLayout != null) {
            tryAgainLayout.setVisibility(View.VISIBLE);
            tvMessage.setTypeface(mTypeface);
            retry.setTypeface(mTypeface);
            tvMessage.setText(resId);
        }
    }

    private void hideTryAgainLayout() {
        if (tryAgainLayout != null) {
            tryAgainLayout.setVisibility(View.GONE);
        }
    }

    private void showProgressBar(boolean show) {
        if (progressBar != null) {
            if (show) {
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setIndeterminate(show);
            } else {
                progressBar.setVisibility(View.GONE);
            }
        }
    }
}
