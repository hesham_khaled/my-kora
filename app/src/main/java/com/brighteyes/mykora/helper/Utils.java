package com.brighteyes.mykora.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.text.format.DateFormat;
import android.util.Log;

import com.brighteyes.mykora.entities.League;
import com.brighteyes.mykora.ui.settings.SettingsHelper;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

/**
 * Created by Shanan on 17/03/2017.
 */

public class Utils {
    public static int getMatchDay(long time, int timeZone) {

        Calendar c0 = Calendar.getInstance();
        c0.add(Calendar.DAY_OF_YEAR, -1); // yesterday

        Calendar c1 = Calendar.getInstance(); // today

        Calendar c2 = Calendar.getInstance();
        c2.add(Calendar.DAY_OF_YEAR, 1); // tomorrow

        Calendar cMatch = Calendar.getInstance();
        cMatch.setTimeInMillis(time * 1000); // your date
        cMatch.set(Calendar.HOUR_OF_DAY, cMatch.get(Calendar.HOUR_OF_DAY) + timeZone);

        if (cMatch.get(Calendar.YEAR) == c0.get(Calendar.YEAR)
                && cMatch.get(Calendar.DAY_OF_YEAR) == c0.get(Calendar.DAY_OF_YEAR)) {
            return CommonConstants.YESTERDAY;
        } else if (cMatch.get(Calendar.YEAR) == c1.get(Calendar.YEAR)
                && cMatch.get(Calendar.DAY_OF_YEAR) == c1.get(Calendar.DAY_OF_YEAR)) {
            return CommonConstants.TODAY;
        } else if (cMatch.get(Calendar.YEAR) == c2.get(Calendar.YEAR)
                && cMatch.get(Calendar.DAY_OF_YEAR) == c2.get(Calendar.DAY_OF_YEAR)) {
            return CommonConstants.TOMORROW;
        } else {
            return -1;
        }
    }

    public static String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        return DateFormat.format("dd/MM/yyyy", cal).toString();
    }

    public static String getTime(long time, int countryTime) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        int hour = cal.get(Calendar.HOUR_OF_DAY) + countryTime;
        cal.set(Calendar.HOUR_OF_DAY, hour);
        return DateFormat.format("hh:mm a", cal).toString();
    }

    public static String getUTCTime(long time) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.setTimeInMillis(time * 1000);
        return DateFormat.format("hh:mm a", cal).toString();
    }

    public static boolean isNetworkAvailable(Context context) {
        if (isNetworkAvailable(context)) {
            try {
                boolean isPingSuccess = new AsyncTask<Void, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(Void... params) {
                        HttpGet httpGet = new HttpGet("http://www.google.com");
                        HttpParams httpParameters = new BasicHttpParams();
                        // Set the timeout in milliseconds until a connection is established.
                        // The default value is zero, that means the timeout is not used.
                        int timeoutConnection = 3000;
                        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
                        // Set the default socket timeout (SO_TIMEOUT)
                        // in milliseconds which is the timeout for waiting for data.
                        int timeoutSocket = 5000;
                        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

                        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
                        try {
                            Log.e("checking", "Checking network connection...");
                            httpClient.execute(httpGet);
                            Log.e("checking", "Connection OK");
                            return true;
                        } catch (ClientProtocolException e) {
                            e.printStackTrace();
                            return false;

                        } catch (IOException e) {
                            e.printStackTrace();
                            return false;
                        }
                    }

                    @Override
                    protected void onPostExecute(Boolean aBoolean) {
                        super.onPostExecute(aBoolean);
                    }
                }.execute().get();
                return isPingSuccess;
            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            } catch (ExecutionException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    public static boolean getNetworkState(Context ctx) {

        ConnectivityManager cm =
                (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                return true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                return true;
            }
        } else {
            return false;
        }
        return false;
    }

    public static boolean isLeaguesEmpty(Collection<League> leagues) {
        for (League l : leagues) {
            if (!l.getMatches().isEmpty()) {
                return false;
            }
        }
        return true;
    }

    public static long getBeginningOfDay(long timeStamp, int timeZone) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timeStamp * 1000);
        cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY) + timeZone);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime().getTime() / 1000;
    }

    public static long applyTimeZone(long matchStartTime, SettingsHelper settingsHelper) {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(matchStartTime * 1000);
        cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY) + settingsHelper.getTimezone());
        return cal.getTime().getTime();
    }

    public static boolean isTimeInFuture(long localStartTime) {
        Calendar cal = Calendar.getInstance();
        boolean isInFuture = cal.getTime().getTime() < localStartTime;
        return isInFuture;
    }
}