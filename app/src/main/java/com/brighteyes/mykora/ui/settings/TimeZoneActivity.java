package com.brighteyes.mykora.ui.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.brighteyes.mykora.R;
import com.brighteyes.mykora.entities.Country;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.helper.Utils;
import com.brighteyes.mykora.ui.base.BaseActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TimeZoneActivity extends BaseActivity {

    @BindView(R.id.time_zone_content)
    ViewGroup contentContainer;
    @BindView(R.id.activity_title)
    TextView activityTitle;
    @BindView(R.id.time_recycler)
    RecyclerView timeZoneRV;
    @BindView(R.id.time_label)
    TextView timeLabel;
    @BindView(R.id.retry)
    TextView retry;
    @BindView(R.id.tvMessage)
    TextView tvMessage;
    @BindView(R.id.tryAgainLayout)
    ViewGroup tryAgainLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    private List<Country> timeZones = new ArrayList<>();
    private TimeAdapter mTimeAdapter;
    private int timeZoneCount = 0;

    public static Intent buildIntent(Context context) {
        return new Intent(context, TimeZoneActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_zone);

    }

    @Override
    protected void onResume() {
        super.onResume();
        ButterKnife.bind(this);

        activityTitle.setTypeface(mTypeface);
        activityTitle.setText(R.string.time_settings);

        timeLabel.setTypeface(mTypeface);
        getTimeZones();
    }

    private void getTimeZones() {

        try {
            if (!Utils.getNetworkState(this)) {
                showTryAgainLayout(R.string.connection_error);
            } else {
                contentContainer.setVisibility(View.GONE);
                showProgressBar(true);

                final boolean[] isLoaded = {false};
                timeZones.clear();
                FirebaseDatabase.getInstance().getReference()
                        .child(CommonConstants.FB_NODE_COUNTRY).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        timeZones.clear();
                        isLoaded[0] = true;
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            Country timeZone = new Country().parseCountry(child);
                            timeZones.add(timeZone);
                        }
                        showProgressBar(false);
                        contentContainer.setVisibility(View.VISIBLE);
                        if (timeZones.size() == 0) {
                            showTryAgainLayout(R.string.no_data);
                        }
                        initTimeZones();
//                        updateTimeZones(CommonConstants.FLAG_INITIALIZE, -1);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                Handler handler = new Handler();

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!isLoaded[0]) {
                            showTryAgainLayout(R.string.no_data);
                            showProgressBar(false);
                        }
                    }
                }, 5000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initTimeZones() {
        timeZoneRV.setHasFixedSize(true);
        mTimeAdapter = new TimeAdapter(this, mSettingsHelper, timeZones);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        timeZoneRV.setLayoutManager(layoutManager);
        timeZoneRV.setAdapter(mTimeAdapter);
    }

    @OnClick(R.id.ic_notifications)
    public void notificationsIconClick() {
        mNavigator.navigateToNotificationActivity();
    }

    @OnClick(R.id.ic_home_action)
    public void logoClick() {
        mNavigator.navigateToMainActivity();
    }

    @OnClick(R.id.retry)
    public void Onclick() {
        showProgressBar(true);
        hideTryAgainLayout();
        contentContainer.setVisibility(View.GONE);
        getTimeZones();
//        recreate();
    }

    private void showTryAgainLayout(int resId) {
        if (tryAgainLayout != null) {
            tryAgainLayout.setVisibility(View.VISIBLE);
            contentContainer.setVisibility(View.GONE);
            tvMessage.setTypeface(mTypeface);
            retry.setTypeface(mTypeface);
            tvMessage.setText(resId);
        }
    }

    private void hideTryAgainLayout() {
        if (tryAgainLayout != null) {
            tryAgainLayout.setVisibility(View.GONE);
            contentContainer.setVisibility(View.VISIBLE);
        }
    }

    private void showProgressBar(boolean show) {
        if (progressBar != null) {
            if (show) {
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setIndeterminate(show);
            } else {
                progressBar.setVisibility(View.GONE);
            }
        }
    }
}
