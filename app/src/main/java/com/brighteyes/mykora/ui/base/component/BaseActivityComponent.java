package com.brighteyes.mykora.ui.base.component;

import com.brighteyes.mykora.ui.base.ActivityScope;
import com.brighteyes.mykora.ui.base.BaseActivity;
import com.brighteyes.mykora.ui.base.module.BaseActivityModule;

import dagger.Subcomponent;


@ActivityScope
@Subcomponent(
        modules = BaseActivityModule.class)
public interface BaseActivityComponent {

    BaseActivity inject(BaseActivity baseActivity);
}