package com.brighteyes.mykora.di.modules;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.brighteyes.mykora.App;
import com.brighteyes.mykora.pref.SharedPreferencesHelper;
import com.brighteyes.mykora.pref.SharedPreferencesHelperImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class StorageModule {

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(App app) {
        return PreferenceManager.getDefaultSharedPreferences(app);
    }

    @Provides
    @Singleton
    SharedPreferencesHelper provideStorageHelper(
            SharedPreferences sharedPreferences) {
        return new SharedPreferencesHelperImpl(sharedPreferences);
    }
}
