package com.brighteyes.mykora.matchesTimes;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;

import com.brighteyes.mykora.entities.Match;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.helper.Utils;
import com.brighteyes.mykora.ui.settings.SettingsHelper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by Shanan on 16/08/2017.
 */

public class PeriodicalAlarmReceiver extends BroadcastReceiver {

    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    ValueEventListener matchesListener;

    @Override
    public void onReceive(Context context, Intent intent) {

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();
        // Put here YOUR code.
        getTodayMatches(context);

        wl.release();
    }

    public void setAlarm(Context context) {

        Log.d(CommonConstants.TAG, "Daily setAlarm !!!");
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, PeriodicalAlarmReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        SettingsHelper settingsHelper = SettingsHelper.getInstance(context);
        long triggerTime = Utils.getBeginningOfDay(System.currentTimeMillis(), settingsHelper.getTimezone());
        am.setInexactRepeating(AlarmManager.RTC, triggerTime, 3600 * 1000L, pi);

    }

    public void cancelAlarm(Context context) {
        Intent intent = new Intent(context, PeriodicalAlarmReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
        databaseReference.removeEventListener(matchesListener);
    }

    private void getTodayMatches(final Context context) {

        matchesListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Match match = new Match().parseMatch(child);
                    long localStartTime = match.getMatchStartTime(SettingsHelper.getInstance(context));
                    if (match.getDay(SettingsHelper.getInstance(context)) == CommonConstants.TODAY && Utils.isTimeInFuture(localStartTime) && match.getMatchStatus() != 2) {
                        MatchStartReceiver startAlarm = new MatchStartReceiver();
                        startAlarm.setAlarm(context, match);

                        MatchEndReceiver endAlarm = new MatchEndReceiver();
                        endAlarm.setAlarm(context, match);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        databaseReference.child(CommonConstants.FB_NODE_MATCH)
                .orderByChild(CommonConstants.FB_MATCH_TIME)
                .addListenerForSingleValueEvent(matchesListener);
    }
}