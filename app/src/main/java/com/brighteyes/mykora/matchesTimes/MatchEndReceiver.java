package com.brighteyes.mykora.matchesTimes;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.brighteyes.mykora.entities.Match;
import com.brighteyes.mykora.fcm.FirebaseHelper;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.ui.settings.SettingsHelper;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Random;


/**
 * Created by Shanan on 16/08/2017.
 */

public class MatchEndReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent i) {
        Log.d(CommonConstants.TAG, "MatchEndReceiver onReceive");
        if (i != null) {
            String matchJSon = i.getStringExtra(CommonConstants.KEY_MATCH);

            try {
                // Update match status on server
                JSONObject dataObject = new JSONObject(matchJSon);
                Match match = new Match().parseMatch(dataObject);
                FirebaseHelper.updateMatchStatusOnServer(match.getId(), 2);
            } catch (Exception e) {
                Log.d(CommonConstants.TAG, e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void setAlarm(Context context, Match match) {

        SettingsHelper settingsHelper = SettingsHelper.getInstance(context);
        long matchStartTime = match.getMatchStartTime(settingsHelper);
        // Set match end time to 4 hours from start time to set match status on firebase to finished
        // if is not finished manually from the admin-panel
        long matchEndTime = matchStartTime + (14400 * 1000);
        Log.d(CommonConstants.TAG, "match end time " + matchEndTime);
        // check that no alarm is created for this match
        if (settingsHelper.getMatchEndTime(match.getId()) == -1) {
            Log.d(CommonConstants.TAG, "No End Alarm was created for this match.");
            setAlarmTime(context, match, matchEndTime, settingsHelper);
        } else {
            Log.d(CommonConstants.TAG, "This match has an End alarm already.");
            // Check that match time is not changed
            long savedTime = settingsHelper.getMatchEndTime(match.getId());
            if (savedTime != matchEndTime) {
                Log.d(CommonConstants.TAG, "Match has an End alarm and Time Changed.");
                // Cancel old alarm
                int requestCode = settingsHelper.getMatchEndRequestCode(match.getId());
                cancelAlarm(context, requestCode);
                // set alarm time to the new time
                setAlarmTime(context, match, matchEndTime, settingsHelper);
            }
            Log.d(CommonConstants.TAG, "Match has an End alarm and Time NOT changed.");
        }
    }

    public void cancelAlarm(Context context, int requestCode) {
        Log.d(CommonConstants.TAG, "Cancel Alarm.");
        Intent intent = new Intent(context, MatchEndReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, requestCode, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    private void setAlarmTime(Context context, Match match, long matchEndTime, SettingsHelper settingsHelper) {
        Log.d(CommonConstants.TAG, "Set End Time.");
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, MatchEndReceiver.class);
        String matchJson = new Gson().toJson(match);
        intent.putExtra(CommonConstants.KEY_MATCH, matchJson);

        int requestCode = new Random().nextInt();
        PendingIntent pendingIntentAlarm = PendingIntent.getBroadcast(context, requestCode, intent, 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, matchEndTime, pendingIntentAlarm);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, matchEndTime, pendingIntentAlarm);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, matchEndTime, pendingIntentAlarm);
        }

        Log.d(CommonConstants.TAG, "Match end time: " + matchEndTime);
        Log.d(CommonConstants.TAG, "End Request Code: " + requestCode);

        saveToSharedPreferences(settingsHelper, match.getId(), matchEndTime, requestCode);
    }

    private void saveToSharedPreferences(SettingsHelper settingsHelper, String matchId, long matchStartTime, int requestCode) {
        // save match id, start time, request code in sharedPreferences

        settingsHelper.setMatchEndTime(matchId, matchStartTime);
        settingsHelper.setMatchEndRequestCode(matchId, requestCode);


    }
}
