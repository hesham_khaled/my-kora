package com.brighteyes.mykora.entities;

import com.brighteyes.mykora.helper.CommonConstants;
import com.google.firebase.database.DataSnapshot;

import org.parceler.Parcel;

/**
 * Created by Shanan on 22/03/2017.
 */
@Parcel
public class Country {
    String flagUrl;
    String name;
    long timeZone;
    String id;

    public Country() {
    }

    public Country parseCountry(DataSnapshot dataSnapshot) {
        this.id = dataSnapshot.getKey();
        this.flagUrl = (String) dataSnapshot.child(CommonConstants.FB_CNTRY_FLAG_URL).getValue();
        this.name = (String) dataSnapshot.child(CommonConstants.FB_CNTRY_NAME).getValue();
        this.timeZone = (Long) dataSnapshot.child(CommonConstants.FB_CNTRY_TIMEZONE).getValue();
        return this;
    }

    public String getId() {
        return id;
    }

    public String getFlagUrl() {
        return flagUrl;
    }

    public String getName() {
        return name;
    }

    public int getTimeZone() {
        return (int) timeZone;
    }
}
