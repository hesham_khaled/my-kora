package com.brighteyes.mykora.ui.settings;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;

import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.matchesTimes.TimingService;
import com.brighteyes.mykora.nav.Navigator.Navigator;
import com.brighteyes.mykora.pref.SharedPreferencesHelper;
import com.brighteyes.mykora.pref.SharedPreferencesHelperImpl;

import java.util.Locale;

public class SettingsHelper {

    private static SettingsHelper helper;
    private final Context context;

    //Notifications Settings
    private final SharedPreferencesHelper sharedPreferencesHelper;

    private SettingsHelper(Context context, SharedPreferencesHelper sharedPreferencesHelper) {
        this.context = context;
        this.sharedPreferencesHelper = sharedPreferencesHelper;
    }

    private SettingsHelper(Context context) {
        this.context = context;
        SharedPreferencesHelper sharedPreferencesHelper =
                new SharedPreferencesHelperImpl(PreferenceManager.getDefaultSharedPreferences(context));
        this.sharedPreferencesHelper = sharedPreferencesHelper;
    }

    public static SettingsHelper getInstance(Context context,
                                             SharedPreferencesHelper sharedPreferencesHelper) {
        if (helper == null) {
            helper = new SettingsHelper(context, sharedPreferencesHelper);
        }
        return helper;
    }

    public static SettingsHelper getInstance(Context context) {
        if (helper == null) {
            helper = new SettingsHelper(context);
        }
        return helper;
    }

    private void setLocale(String lang, boolean restart, Navigator navigator) {
        //setPrefLang(lang);
        Locale locale = new Locale(lang);
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = locale;
        res.updateConfiguration(conf, dm);
        if (restart) {
            //
        }
    }

    public void changeTimezone(int timezone, int countryIndex, Activity activity) {
        setTimezone(timezone, countryIndex);
        startTimingService();
        activity.setResult(Activity.RESULT_OK);
        activity.finish();
    }

    private void startTimingService() {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (TimingService.class.getName().equals(service.service.getClassName())) {
                return;
            }
        }
        context.startService(new Intent(context, TimingService.class));
    }

    private void setTimezone(int timezone, int countryIndex) {
        sharedPreferencesHelper.setInt(CommonConstants.KEY_TIMEZONE, timezone);
        sharedPreferencesHelper.setInt(CommonConstants.KEY_COUNTRY_INDEX, countryIndex);
    }

    public int getTimezoneIndex() {
        return sharedPreferencesHelper.getInt(CommonConstants.KEY_COUNTRY_INDEX);
    }

    public int getTimezone() {
        return sharedPreferencesHelper.getInt(CommonConstants.KEY_TIMEZONE);
    }

    public boolean isMatchNotificationEnable() {
        return sharedPreferencesHelper.getBoolean(CommonConstants.KEY_MATCH_NOTIFICATION_ENABLE, true);
    }

    public int getMatchNotificationSound() {
        return sharedPreferencesHelper.getInt(CommonConstants.KEY_MATCH_NOTIFICATION_SOUND);
    }

    public void setMatchNotificationEnable(boolean enable, int sound) {
        Log.d("mSettings", " match sound: " + sound);
        sharedPreferencesHelper.setBoolean(CommonConstants.KEY_MATCH_NOTIFICATION_ENABLE, enable);
        sharedPreferencesHelper.setInt(CommonConstants.KEY_MATCH_NOTIFICATION_SOUND, sound);
    }

    public boolean isNewsNotificationEnable() {
        return sharedPreferencesHelper.getBoolean(CommonConstants.KEY_NEWS_NOTIFICATION_ENABLE, true);
    }

    public int getNewsNotificationSound() {
        return sharedPreferencesHelper.getInt(CommonConstants.KEY_NEWS_NOTIFICATION_SOUND);
    }

    public void setNewsNotificationEnable(boolean enable, int sound) {
        sharedPreferencesHelper.setBoolean(CommonConstants.KEY_NEWS_NOTIFICATION_ENABLE, enable);
        sharedPreferencesHelper.setInt(CommonConstants.KEY_NEWS_NOTIFICATION_SOUND, sound);
    }

    public boolean isGoalNotificationEnable() {
        return sharedPreferencesHelper.getBoolean(CommonConstants.KEY_GOAL_NOTIFICATION_ENABLE, true);
    }

    public int getGoalNotificationSound() {
        return sharedPreferencesHelper.getInt(CommonConstants.KEY_GOAL_NOTIFICATION_SOUND);
    }

    public void setGoalNotificationEnable(boolean enable, int sound) {
        Log.d("mSettings", " goals sound: " + sound);
        sharedPreferencesHelper.setBoolean(CommonConstants.KEY_GOAL_NOTIFICATION_ENABLE, enable);
        sharedPreferencesHelper.setInt(CommonConstants.KEY_GOAL_NOTIFICATION_SOUND, sound);
    }

    public boolean isFcmTokenSet() {
        return sharedPreferencesHelper.getBoolean(CommonConstants.KEY_IS_FCM_TOKEN_SET);
    }

    public String getFcmTokenKey() {
        return sharedPreferencesHelper.getString(CommonConstants.KEY_FCM_TOKEN);
    }

    public boolean isVideoNotificationEnable() {
        return sharedPreferencesHelper.getBoolean(CommonConstants.KEY_VIDEO_NOTIFICATION_ENABLE, true);
    }

    public int getVideoNotificationSound() {
        return sharedPreferencesHelper.getInt(CommonConstants.KEY_VIDEO_NOTIFICATION_SOUND);
    }

    public void setVideoNotificationEnable(boolean enable, int sound) {
        sharedPreferencesHelper.setBoolean(CommonConstants.KEY_VIDEO_NOTIFICATION_ENABLE, enable);
        sharedPreferencesHelper.setInt(CommonConstants.KEY_VIDEO_NOTIFICATION_SOUND, sound);
    }

    public String getLang() {
        return context.getResources().getConfiguration().locale.getLanguage();
    }

    public void setMatchStartTime(String matchId, long alarmTime) {
        Log.d(CommonConstants.TAG, "Saving " + matchId + " @ " + alarmTime);
        sharedPreferencesHelper.setLong(matchId, alarmTime);
    }

    public long getMatchStartTime(String matchId) {
        return sharedPreferencesHelper.getLong(matchId);
    }

    public void setMatchStartRequestCode(String matchId, int requestCode) {
        Log.d(CommonConstants.TAG, "Saving " + matchId + " @code " + requestCode);
        sharedPreferencesHelper.setInt(matchId + "_code", requestCode);
    }

    public int getMatchStartRequestCode(String matchId) {
        return sharedPreferencesHelper.getInt(matchId + "_code");
    }

    public void setMatchEndTime(String matchId, long alarmTime) {
        sharedPreferencesHelper.setLong(matchId + "_end", alarmTime);
    }

    public long getMatchEndTime(String matchId) {
        return sharedPreferencesHelper.getLong(matchId + "_end");
    }

    public void setMatchEndRequestCode(String matchId, int requestCode) {
        sharedPreferencesHelper.setInt(matchId + "_end_code", requestCode);
    }

    public int getMatchEndRequestCode(String matchId) {
        return sharedPreferencesHelper.getInt(matchId + "_end_code");
    }
}