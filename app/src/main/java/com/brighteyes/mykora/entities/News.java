package com.brighteyes.mykora.entities;

import com.brighteyes.mykora.helper.CommonConstants;
import com.google.firebase.database.DataSnapshot;

import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcel;

/**
 * Created by Shanan on 15/02/2017.
 */
@Parcel
public class News {
    String id;
    String creationDate;
    String details;
    boolean isImportant;
    String photo_url;
    String tags;
    String title;
    String catId;
    String catName;

    public News() {
    }

    public News parseNews(DataSnapshot dataSnapshot) {
        this.id = dataSnapshot.getKey();

        this.details = (String) dataSnapshot.child(CommonConstants.FB_NEWS_DETAILS).getValue();
        this.isImportant = (boolean) dataSnapshot.child(CommonConstants.FB_NEWS_IS_IMPORTANT)
                .getValue();
        this.photo_url = (String) dataSnapshot.child(CommonConstants.FB_NEWS_PHOTO_URL).getValue();
        this.tags = (String) dataSnapshot.child(CommonConstants.FB_NEWS_TAGS).getValue();
        this.title = (String) dataSnapshot.child(CommonConstants.FB_NEWS_TITLE).getValue();
        this.catId = (String) dataSnapshot.child(CommonConstants.FB_NEWS_CATEGORY_ID).getValue();
        this.catName = (String) dataSnapshot.child(CommonConstants.FB_NEWS_CATEGORY_NAME).getValue();

        return this;
    }

    public String getId() {
        return id;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public String getDetails() {
        return details;
    }

    public String getTitle() {
        return title;
    }

    public String getTags() {
        return tags;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public boolean isImportant() {
        return isImportant;
    }

    public String getCatId() {
        return catId;
    }

    public String getCatName() {
        return catName;
    }

    public News parseNews(JSONObject newsObject) throws JSONException {
        News news = new News();
        news.id = newsObject.getString("$id");
        news.details = newsObject.getString(CommonConstants.FB_NEWS_DETAILS);
        news.isImportant = newsObject.getBoolean(CommonConstants.FB_NEWS_IS_IMPORTANT);
        news.photo_url = newsObject.getString(CommonConstants.FB_NEWS_PHOTO_URL);
        news.tags = newsObject.getString(CommonConstants.FB_NEWS_TAGS);
        news.title = newsObject.getString(CommonConstants.FB_NEWS_TITLE);
        news.catId = newsObject.getString(CommonConstants.FB_NEWS_CATEGORY_ID);
        news.catName = newsObject.getString(CommonConstants.FB_NEWS_CATEGORY_NAME);

        return news;
    }
}