package com.brighteyes.mykora.ui.matchInfo;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.brighteyes.mykora.R;
import com.brighteyes.mykora.entities.Match;
import com.brighteyes.mykora.entities.Video;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.ui.base.BaseSupportFragment;
import com.brighteyes.mykora.ui.videos.VideosAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FinishedMatchFragment extends BaseSupportFragment {
    @BindView(R.id.tvMessage)
    TextView tvMessage;

    @BindView(R.id.tryAgainLayout)
    ViewGroup tryAgainLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.match_videos)
    ViewGroup matchVideosLayout;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.finished_title)
    TextView finishedTitle;
    @BindView(R.id.match_videos_title)
    TextView videosTitle;

    private List<Video> matchVideos = new ArrayList<>();
    private VideosAdapter mVideosAdapter;
    private int videosCount = 0;
    private Match mMatch;

    public FinishedMatchFragment() {
        // Required empty public constructor
    }

    public static FinishedMatchFragment newInstance(Match match) {
        FinishedMatchFragment fragment = new FinishedMatchFragment();
        Bundle args = new Bundle();
        args.putParcelable(CommonConstants.KEY_MATCH, Parcels.wrap(match));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_finished_match, container, false);
        ButterKnife.bind(this, rootView);

        finishedTitle.setTypeface(mTypeface);
        videosTitle.setTypeface(mTypeface);

        if (getArguments() != null) {
            Bundle bundle = getArguments();
            mMatch = Parcels.unwrap(bundle.getParcelable(CommonConstants.KEY_MATCH));
            if (mMatch != null) {
                getMatchVideos(mMatch);
            } else {
                showTryAgainLayout(R.string.no_match_videos);
            }
        }
        return rootView;
    }

    private void getMatchVideos(final Match match) {
        hideTryAgainLayout();
        showProgressBar(true);

        FirebaseDatabase.getInstance()
                .getReference()
                .child(CommonConstants.FB_NODE_VIDEO)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        matchVideos.clear();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            Video video = new Video().parseVideo(child);
                            if (isVideoRelatedToMatch(video, match)) {
                                matchVideos.add(video);
                            }
                        }
                        showProgressBar(false);
                        hideTryAgainLayout();

                        if (matchVideos.size() > 0) {
                            updateVideos(CommonConstants.FLAG_INITIALIZE, -1, match);
                        } else {
                            showTryAgainLayout(R.string.no_match_videos);
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void updateVideos(int transaction, int itemPosition, Match match) {
        switch (transaction) {
            case CommonConstants.FLAG_INITIALIZE:
                initVideosRV();
                try {
                    addVideoChildValueListener(match);
                } catch (Exception ex) {
                    Log.d(CommonConstants.TAG, ex.getLocalizedMessage());
                }
                break;
            case CommonConstants.FLAG_INSERT:
                mVideosAdapter.notifyItemInserted(matchVideos.size() - 1);
                break;
            case CommonConstants.FLAG_UPDATE:
                mVideosAdapter.notifyItemChanged(itemPosition);
                break;
            case CommonConstants.FLAG_DELETE:
                mVideosAdapter.notifyItemRemoved(itemPosition);
                break;
            default:
                break;
        }
    }

    private void initVideosRV() {
        recyclerView.setHasFixedSize(true);
        mVideosAdapter = new VideosAdapter(getActivity(), matchVideos, false, mNavigator);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mVideosAdapter);
    }

    private void addVideoChildValueListener(final Match match) {

        FirebaseDatabase.getInstance().getReference()
                .child(CommonConstants.FB_NODE_VIDEO)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        videosCount++;
                        if (videosCount > matchVideos.size()) {
                            Video video = new Video().parseVideo(dataSnapshot);
                            if (isVideoRelatedToMatch(video, match)) {
                                matchVideos.add(video);
                                updateVideos(CommonConstants.FLAG_INSERT, matchVideos.size() - 1, match);
                            }
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        Video video = new Video().parseVideo(dataSnapshot);
                        if (isVideoRelatedToMatch(video, match)) {
                            int position = getVideoPosition(video.getId());
                            matchVideos.set(position, video);
                            updateVideos(CommonConstants.FLAG_UPDATE, position, match);
                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        Video video = new Video().parseVideo(dataSnapshot);
                        if (isVideoRelatedToMatch(video, match)) {
                            int position = getVideoPosition(dataSnapshot.getKey());
                            matchVideos.remove(position);
                            updateVideos(CommonConstants.FLAG_DELETE, position, match);
                        }
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
    }

    private int getVideoPosition(String id) {
        int index = -1;
        for (Video video : matchVideos) {
            if (!video.getId().equals("") && video.getId().equals(id)) {
                index = matchVideos.indexOf(video);
                break;
            }
        }
        return index;
    }


    private boolean isVideoRelatedToMatch(Video video, Match m) {
        String[] vid1array = video.getTags().split(",");
        for (int i = 0; i < vid1array.length; i++) {
            vid1array[i] = vid1array[i].trim();
        }
        Set<String> video1tagsSet = new HashSet<>(Arrays.asList(vid1array));

        String[] vid2array = m.getTags().split(",");
        for (int i = 0; i < vid2array.length; i++) {
            vid2array[i] = vid2array[i].trim();
        }
        Set<String> video2tagsSet = new HashSet<>(Arrays.asList(vid2array));

        video1tagsSet.retainAll(video2tagsSet);
        return video1tagsSet.size() > 0;
    }

    private void showTryAgainLayout(int resId) {
        if (tryAgainLayout != null) {
            tryAgainLayout.setVisibility(View.VISIBLE);
            matchVideosLayout.setVisibility(View.GONE);
            tvMessage.setTypeface(mTypeface);
            tvMessage.setText(resId);
        }
    }

    private void hideTryAgainLayout() {
        if (tryAgainLayout != null) {
            tryAgainLayout.setVisibility(View.GONE);
            matchVideosLayout.setVisibility(View.VISIBLE);
        }
    }

    private void showProgressBar(boolean show) {
        if (progressBar != null) {
            if (show) {
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setIndeterminate(show);
            } else {
                progressBar.setVisibility(View.GONE);
            }
        }
    }
}
