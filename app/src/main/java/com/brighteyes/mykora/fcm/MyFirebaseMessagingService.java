package com.brighteyes.mykora.fcm;

import android.util.Log;

import com.brighteyes.mykora.entities.Notification;
import com.brighteyes.mykora.helper.UIHelper;
import com.brighteyes.mykora.ui.settings.SettingsHelper;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by Shanan on 28/03/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Notification MessageData getData: " + remoteMessage.getData());
        Log.d(TAG, "Notification MessageData getSentTime: " + remoteMessage.getSentTime());
        Log.d(TAG, "Notification MessageData getMessageId: " + remoteMessage.getMessageId());
        Log.d(TAG, "Notification MessageData getMessageType: " + remoteMessage.getMessageType());

        boolean isMatchNotificationEnabled = SettingsHelper.getInstance(getApplicationContext()).isMatchNotificationEnable();
        boolean isNewsNotificationEnabled = SettingsHelper.getInstance(getApplicationContext()).isNewsNotificationEnable();
        boolean isGoalNotificationEnabled = SettingsHelper.getInstance(getApplicationContext()).isGoalNotificationEnable();
        boolean isVideoNotificationEnabled = SettingsHelper.getInstance(getApplicationContext()).isVideoNotificationEnable();

        int matchNotificationSound = SettingsHelper.getInstance(getApplicationContext()).getMatchNotificationSound();
        int newsNotificationSound = SettingsHelper.getInstance(getApplicationContext()).getNewsNotificationSound();
        int goalNotificationSound = SettingsHelper.getInstance(getApplicationContext()).getGoalNotificationSound();
        int videoNotificationSound = SettingsHelper.getInstance(getApplicationContext()).getVideoNotificationSound();

        final Notification notification = Notification.getNotification(remoteMessage.getData());

        if (notification != null) {
            if (notification.isMatchNotification() && isMatchNotificationEnabled) {
                UIHelper.showNotification(getApplicationContext(), notification, matchNotificationSound);
            } else if (notification.isNewsNotification() && isNewsNotificationEnabled) {
                UIHelper.showNotification(getApplicationContext(), notification, newsNotificationSound);
            } else if (notification.isVideoNotification() && isVideoNotificationEnabled) {
                UIHelper.showNotification(getApplicationContext(), notification, videoNotificationSound);
            } else if (notification.isGoalNotification() && isGoalNotificationEnabled) {
                UIHelper.showNotification(getApplicationContext(), notification, goalNotificationSound);
            }
        }
    }
}
