package com.brighteyes.mykora.matchesTimes;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.brighteyes.mykora.helper.CommonConstants;

/**
 * Created by Shanan on 16/08/2017.
 */

public class TimingService extends Service {

    private PeriodicalAlarmReceiver alarm = new PeriodicalAlarmReceiver();

    public void onCreate() {
        Log.d(CommonConstants.TAG, "My Kora Timing Service onCreate");
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(CommonConstants.TAG, "My Kora Timing Service onStartCommand");
        alarm.setAlarm(this);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
