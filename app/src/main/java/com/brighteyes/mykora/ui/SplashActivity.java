package com.brighteyes.mykora.ui;

import android.os.Bundle;
import android.util.Log;

import com.brighteyes.mykora.R;
import com.brighteyes.mykora.Singleton;
import com.brighteyes.mykora.entities.GoogleAdsId;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.ui.base.BaseActivity;
import com.github.silvestrpredko.dotprogressbar.DotProgressBar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        try {
            getGoogleAdsIDs();
        } catch (Exception e) {
            Log.d(CommonConstants.TAG, e.getMessage());
            e.printStackTrace();
        }
        DotProgressBar dotProgressBar = (DotProgressBar) findViewById(R.id.dot_progress_bar);
        dotProgressBar.postDelayed(new Runnable() {
            @Override
            public void run() {
                mNavigator.navigateToMainActivity();
            }
        }, 2000);
    }

    public void getGoogleAdsIDs() throws Exception {
        FirebaseDatabase.getInstance().getReference().child(CommonConstants.FB_NODE_GOOGLE_ADS_IDS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    if (child != null) {
                        Singleton.getInstance().setAdsIds(new GoogleAdsId().parseAdsId(child));
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}