package com.brighteyes.mykora.ui.base;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.brighteyes.mykora.App;
import com.brighteyes.mykora.Singleton;
import com.brighteyes.mykora.nav.Navigator.Navigator;
import com.brighteyes.mykora.pref.SharedPreferencesHelper;
import com.brighteyes.mykora.ui.settings.SettingsHelper;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import javax.inject.Inject;

public class BaseFragment extends Fragment {

    @Inject
    public Navigator mNavigator;
    @Inject
    public SharedPreferencesHelper mSharedPreferencesHelper;

    protected SettingsHelper mSettingsHelper;
    //private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getAppComponent().inject(this);
        //App.get(getActivity()).getAppComponent().inject(this);

        mNavigator.setContext(getActivity());
        mSettingsHelper = SettingsHelper.getInstance(getActivity(), mSharedPreferencesHelper);
    }

    protected View getLayout(@LayoutRes int layoutResID, LayoutInflater inflater,
                             ViewGroup container) {
        View rootView = inflater.inflate(layoutResID, container, false);

        //unbinder = ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //if (unbinder != null) {
        //  unbinder.unbind();
        //}
    }

    protected void loadAd(RelativeLayout adContainer) {

        AdRequest adRequest = new AdRequest.Builder().build();

        AdView mAdView = new AdView(getActivity());
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId(Singleton.getInstance().getAdsIds().getBannerAdId());
        adContainer.addView(mAdView);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.d("Hesham", "ad loaded");
            }
        });
        mAdView.loadAd(adRequest);
    }
}
