package com.brighteyes.mykora.ui.base;

import android.app.Fragment;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.brighteyes.mykora.App;
import com.brighteyes.mykora.R;
import com.brighteyes.mykora.Singleton;
import com.brighteyes.mykora.nav.Navigator.Navigator;
import com.brighteyes.mykora.pref.SharedPreferencesHelper;
import com.brighteyes.mykora.ui.base.module.BaseActivityModule;
import com.brighteyes.mykora.ui.custom.CustomTabLayout;
import com.brighteyes.mykora.ui.helper.ActivityHelper;
import com.brighteyes.mykora.ui.settings.SettingsHelper;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BaseActivity extends AppCompatActivity {

    @Inject
    public Navigator mNavigator;
    @Inject
    public SharedPreferencesHelper mSharedPreferencesHelper;
    @Inject
    public ActivityHelper mActivityHelper;
    protected SettingsHelper mSettingsHelper;
    protected Typeface mTypeface;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getApplication()).getAppComponent().plus(new BaseActivityModule(this)).inject(this);
        //App.get(this).getAppComponent().plus(new BaseActivityModule(this)).inject(this);

        mNavigator.setContext(this);
        mTypeface = App.defaultFont;
        mSettingsHelper = SettingsHelper.getInstance(this, mSharedPreferencesHelper);
        checkNetWork();
    }

    private void checkNetWork() {
        //if (!Utility.getNetworkState(this)) {
        //  toastShort(getString(R.string.no_connection));
        //}
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);

        unbinder = ButterKnife.bind(this);
    }

    protected void showTitle() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }
    }

    protected void hideTitle() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    protected void showNavigationIcon() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    protected void hideNavigationIcon() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    public void showLoader(CircularProgressView progressView) {
        progressView.setVisibility(View.VISIBLE);
        progressView.startAnimation();
    }

    public void stopLoader(CircularProgressView progressView) {
        progressView.setVisibility(View.GONE);
        progressView.stopAnimation();
    }

    /**
     * Gets the activity helper associated with this Activity.
     *
     * @return The current {@link ActivityHelper} instance.
     */
    protected ActivityHelper getActivityHelper() {
        return mActivityHelper;
    }

    /**
     * Shows a toast for a short amount of time (3 seconds).
     *
     * @param message The message to show
     */
    protected void toastShort(String message) {
        mActivityHelper.toastShort(message);
    }

    /**
     * Shows a toast for a long amount of time (5 seconds).
     *
     * @param message The message to show
     */
    protected void toastLong(String message) {
        mActivityHelper.toastLong(message);
    }

    /**
     * Adds a Fragment to this activity
     *
     * @param fragment    The fragment to add.
     * @param viewGroupId Viewgroup which will be used to embed the fragment.
     */
    protected void addFragment(Fragment fragment, int viewGroupId) {
        mActivityHelper.addFragment(fragment, viewGroupId);
    }

    /**
     * Removes a Fragment to this activity
     *
     * @param fragment The fragment to remove.
     */
    protected void removeFragment(Fragment fragment) {
        mActivityHelper.removeFragment(fragment);
    }

    /**
     * Replaces a Fragment in this activity with another one.
     *
     * @param fragment       The fragment to add.
     * @param viewGroupId    Viewgroup which will be used to embed the fragment.
     * @param addToBackStack True if the fragment should be added to the backstack, otherwise false.
     */
    protected void replaceFragment(Fragment fragment, int viewGroupId, boolean addToBackStack) {
        mActivityHelper.replaceFragment(fragment, viewGroupId, addToBackStack);
    }

    protected void addDividers(CustomTabLayout mainTabLayout) {

        LinearLayout linearLayout = (LinearLayout) mainTabLayout.getChildAt(0);
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(Color.BLACK);
        drawable.setSize(1, 1);
        linearLayout.setDividerPadding(20);
        linearLayout.setDividerDrawable(drawable);
    }

    protected void displayImagePicasso(final String s, final ImageView imageView) {
        Picasso.with(BaseActivity.this).setLoggingEnabled(true);
        Picasso.with(BaseActivity.this)
                .load(s)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .placeholder(R.drawable.logo_transparent)
                .fit()
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {

                        //Try again online if cache failed
                        Picasso.with(BaseActivity.this)
                                .load(s)
                                .error(R.drawable.logo_transparent)
                                .placeholder(R.drawable.logo_transparent)
                                .into(imageView, new Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError() {
                                        Log.v("Picasso", "Could not fetch image");
                                    }
                                });
                    }
                });
    }

    protected void loadAd(RelativeLayout adContainer) {

        final AdRequest adRequest = new AdRequest.Builder().build();

        final AdView mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        if (Singleton.getInstance().getAdsIds() != null) {
            mAdView.setAdUnitId(Singleton.getInstance().getAdsIds().getBannerAdId());
        }
        adContainer.addView(mAdView);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.loadAd(adRequest);
            }
        });
    }
}