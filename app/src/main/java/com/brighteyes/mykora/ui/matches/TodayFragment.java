package com.brighteyes.mykora.ui.matches;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.brighteyes.mykora.R;
import com.brighteyes.mykora.entities.League;
import com.brighteyes.mykora.entities.Match;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.helper.Utils;
import com.brighteyes.mykora.ui.base.BaseSupportFragment;
import com.brighteyes.mykora.ui.custom.SpacesItemDecoration;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TodayFragment extends BaseSupportFragment {

    @BindView(R.id.recyclerView)
    RecyclerView leaguesRV;
    Map<String, League> mLeagues = new HashMap<>();
    @BindView(R.id.tvMessage)
    TextView tvMessage;
    @BindView(R.id.retry)
    TextView retry;
    @BindView(R.id.tryAgainLayout)
    ViewGroup tryAgainLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private SectionsAdapter mLeaguesAdapter;


    public TodayFragment() {
        // Required empty public constructor
    }

    public static TodayFragment newInstance(String title) {
        TodayFragment fragment = new TodayFragment();
        fragment.setTitle(title);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_recycler, container, false);
        ButterKnife.bind(this, rootView);

        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.match_item_space);
        leaguesRV.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        initRV(new ArrayList<League>());
        getLeagues();
        return rootView;
    }

    private void getLeagues() {

        try {
            if (!Utils.getNetworkState(getActivity())) {
                showTryAgainLayout(R.string.no_matches);
            } else {

                hideTryAgainLayout();
                showProgressBar(true);

                final boolean[] isLoaded = {false};

                FirebaseDatabase.getInstance().getReference()
                        .child(CommonConstants.FB_NODE_CHAMPIONSHIP)
                        .addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                mLeagues.clear();
                                isLoaded[0] = true;

                                for (DataSnapshot child : dataSnapshot.getChildren()) {
                                    League league = new League().parseLeague(child);
                                    getLeagueMatches(league);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                showTryAgainLayout(R.string.no_matches);
                                showProgressBar(false);
                            }
                        });

                Handler handler = new Handler();

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!isLoaded[0]) {
                            showTryAgainLayout(R.string.no_matches);
                            showProgressBar(false);
                        }
                    }
                }, 5000);
            }
        } catch (Exception e) {
            //  e.printStackTrace();
            showTryAgainLayout(R.string.no_matches);
        }

    }

    private void getLeagueMatches(final League l) {

        FirebaseDatabase.getInstance().getReference()
                .child(CommonConstants.FB_NODE_MATCH)
                .orderByChild(CommonConstants.FB_MATCH_CHAMP_ID).equalTo(l.getId())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        showProgressBar(false);
                        hideTryAgainLayout();

                        l.getMatches().clear();

                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            Match match = new Match().parseMatch(child);
                            if (match.getDay(mSettingsHelper) == CommonConstants.TODAY) {
                                l.getMatches().add(match);
                            }
                        }
                        if (mLeagues.containsKey(l.getId())) {
                            mLeagues.get(l.getId()).setMatches(l.getMatches());
                        } else {
                            mLeagues.put(l.getId(), l);
                        }
                        mLeaguesAdapter.setLeagues(new ArrayList<>(mLeagues.values()));

                        if (mLeagues.isEmpty() || Utils.isLeaguesEmpty(mLeagues.values())) {
                            showProgressBar(false);
                            showTryAgainLayout(R.string.no_matches);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void initRV(List<League> leagues) {

        mLeaguesAdapter = new SectionsAdapter(getActivity(), leagues, mNavigator, mSettingsHelper);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        leaguesRV.setLayoutManager(layoutManager);
        mLeaguesAdapter.setLayoutManager(layoutManager);
        leaguesRV.setAdapter(mLeaguesAdapter);
    }

    @OnClick(R.id.retry)
    public void Onclick() {
        initRV(new ArrayList<League>());
        getLeagues();
    }

    private void showTryAgainLayout(int resId) {
        if (tryAgainLayout != null) {
            tryAgainLayout.setVisibility(View.VISIBLE);
            tvMessage.setTypeface(mTypeface);
            retry.setTypeface(mTypeface);
            tvMessage.setText(resId);
        }
    }

    private void hideTryAgainLayout() {
        if (tryAgainLayout != null) {
            tryAgainLayout.setVisibility(View.GONE);
        }
    }

    private void showProgressBar(boolean show) {
        if (progressBar != null) {
            if (show) {
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setIndeterminate(show);
            } else {
                progressBar.setVisibility(View.GONE);
            }
        }
    }
}