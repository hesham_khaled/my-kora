package com.brighteyes.mykora.ui.matchInfo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.brighteyes.mykora.R;
import com.brighteyes.mykora.entities.MediaSource;
import com.brighteyes.mykora.entities.Video;
import com.brighteyes.mykora.helper.CommonConstants;
import com.brighteyes.mykora.helper.UIHelper;
import com.brighteyes.mykora.ui.base.BaseSupportFragment;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.EMVideoView;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoViewFragment extends BaseSupportFragment implements OnPreparedListener {

    private static final String ARG_MEDIA_SOURCE = "media_source";
    private static boolean m_iAmVisible;
    @BindView(R.id.videoView)
    EMVideoView videoView;
    private MediaSource mediaSource;
    private boolean m_isVideoPrepared;

    public VideoViewFragment() {
        // Required empty public constructor
    }

    public static VideoViewFragment newInstance(MediaSource mediaSource) {
        VideoViewFragment fragment = new VideoViewFragment();
        fragment.setTitle(mediaSource.getName());
        Bundle args = new Bundle();
        args.putParcelable(ARG_MEDIA_SOURCE, Parcels.wrap(mediaSource));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUserVisibleHint(false);
        if (getArguments() != null) {
            mediaSource = Parcels.unwrap(getArguments().getParcelable(ARG_MEDIA_SOURCE));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.video_view, container, false);
        ButterKnife.bind(this, rootView);
        initVideo();
        addFullScreenButton();
        return rootView;
    }

    private void addFullScreenButton() {
        Button fullScreenBtn = new Button(getActivity());
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(32, 32);
        int dp8 = UIHelper.convertToPx(8, getActivity());
        llp.setMarginStart(dp8);
        fullScreenBtn.setPadding(dp8, 0, dp8, dp8);
        fullScreenBtn.setLayoutParams(llp);
        fullScreenBtn.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ic_full));
        fullScreenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Video video = new Video();
                video.setThumb_url(mediaSource.getThumbnail());
                video.setVideoUrl(mediaSource.getBody());
                    /* temp for debugging*/
                mNavigator.navigateToFullScreen(video, videoView.getCurrentPosition(), videoView.isPlaying());
            }
        });

        videoView.getVideoControls().addExtraView(fullScreenBtn);
    }

    public void initVideo() {
        if (videoView != null) {
            videoView.setReleaseOnDetachFromWindow(true);
            videoView.setPreviewImage(Uri.parse(mediaSource.getThumbnail()));
            videoView.setVideoURI(Uri.parse(mediaSource.getBody()));
            videoView.setOnPreparedListener(this);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        m_iAmVisible = isVisibleToUser;
        if (m_iAmVisible) {
            Log.d("FragVisibility", mediaSource != null ? mediaSource.getName() : "frag" + " is now visible");
            initVideo();
            startPlaying();
        } else {
            stopPlaying();
        }
    }

    @Override
    public void onPrepared() {
        m_isVideoPrepared = true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CommonConstants.FULL_SCREEN_CODE) {
            startPlaying();
        }
    }

    private void stopPlaying() {
        if (videoView != null) {
            videoView.stopPlayback();
        }
    }

    private void startPlaying() {
        if (videoView != null && m_isVideoPrepared) {
            videoView.start();
        }
    }
}