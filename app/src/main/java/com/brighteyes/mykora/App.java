package com.brighteyes.mykora;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;

import com.brighteyes.mykora.di.component.AppComponent;
import com.brighteyes.mykora.di.component.DaggerAppComponent;
import com.brighteyes.mykora.di.modules.AppModule;
import com.brighteyes.mykora.di.modules.StorageModule;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.FirebaseApp;

import dagger.Module;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Shanan on 01/02/2017.
 */
@Module
public class App extends Application {
    public static Typeface defaultFont;
    AppComponent appComponent;

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        FirebaseApp.initializeApp(this);
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .storageModule(new StorageModule())
                .build();
        defaultFont = Typeface.createFromAsset(getAssets(), "fonts/stc.otf");
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
