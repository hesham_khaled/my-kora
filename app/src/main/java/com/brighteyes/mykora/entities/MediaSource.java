package com.brighteyes.mykora.entities;

import com.brighteyes.mykora.helper.CommonConstants;
import com.google.firebase.database.DataSnapshot;

import org.parceler.Parcel;

/**
 * Created by Shanan on 22/03/2017.
 */
@Parcel
public class MediaSource {
    String id;
    String body;
    boolean isDefault;
    String name;
    long type;
    String thumbnail;

    public MediaSource() {

    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public MediaSource parseMediaSource(DataSnapshot dataSnapshot) {
        this.id = dataSnapshot.getKey();
        this.body = (String) dataSnapshot.child(CommonConstants.FB_CH_MEDIA_BODY).getValue();
        this.isDefault = (boolean) dataSnapshot.child(CommonConstants.FB_CH_MEDIA_IS_DEFAULT)
                .getValue();
        this.name = (String) dataSnapshot.child(CommonConstants.FB_CH_MEDIA_NAME).getValue();
        this.type = (Long) dataSnapshot.child(CommonConstants.FB_CH_MEDIA_TYPE).getValue();
        return this;
    }

    public String getBody() {
        return body;
    }

    public String getId() {
        return id;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public String getName() {
        return name;
    }

    public long getType() {
        return type;
    }
}
