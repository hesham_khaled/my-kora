package com.brighteyes.mykora.helper;

/**
 * Created by Shanan on 20/02/2017.
 */

public class CommonConstants {
    public final static String KEY_MATCH = "match";
    public final static String TAG = "MyKora";
    public final static String KEY_FIRST_RUN = "first_run";
    public final static String KEY_IS_FCM_TOKEN_SET = "token_set";
    public final static String KEY_FCM_TOKEN = "fcm_token";

    public static final int FLAG_INITIALIZE = 0;
    public static final int FLAG_INSERT = 1;
    public static final int FLAG_UPDATE = 2;
    public static final int FLAG_DELETE = 3;

    public static final int MATCH_STATUS_NOT_STARTED = 0;
    public static final int MATCH_STATUS_LIVE = 1;
    public static final int MATCH_STATUS_FINISHED = 2;

    // Firebase
    // outer nodes
    public final static String FB_NODE_CATEGORY_NEWS = "category_news";
    public final static String FB_NODE_NEWS = "news";
    public final static String FB_NODE_NEWS_CATEGORY = "news_category";
    public final static String FB_NODE_CATEGORY_VIDEOS = "category_videos";
    public final static String FB_NODE_VIDEO = "video";
    public final static String FB_NODE_VIDEO_CATEGORY = "video_category";
    public final static String FB_NODE_CHAMPIONSHIP = "champion";
    public final static String FB_NODE_CHANNEL = "channel";
    public final static String FB_NODE_CHANNEL_MEDIA = "channel_media";
    public final static String FB_NODE_CLUB = "club";
    public final static String FB_NODE_COMMENTATOR = "commentator";
    public final static String FB_NODE_COUNTRY = "country";
    public final static String FB_NODE_MATCH = "match";
    public final static String FB_NODE_REFEREE = "referee";
    public final static String FB_NODE_STADIUM = "stadium";
    public final static String FB_NODE_FCM_TOKENS = "fcm_tokens";
    public final static String FB_NODE_GOOGLE_ADS_IDS = "googleAdsId";
    // FB keys
    // category news
    public final static String FB_CAT_NEWS_NAME = "category_name";
    public final static String FB_CAT_NEWS_DETAILS = "details";
    public final static String FB_CAT_NEWS_IS_IMPORTANT = "is_important";
    public final static String FB_CAT_NEWS_PHOTO_URL = "photo_url";
    public final static String FB_CAT_NEWS_TAGS = "tags";
    public final static String FB_CAT_NEWS_TITLE = "title";
    // category video
    public final static String FB_CAT_VID_CREATION_DATE = "creation_date";
    public final static String FB_CAT_VID_DESCRIPTION = "description";
    public final static String FB_CAT_VID_IS_IMPORTANT = "is_important";
    public final static String FB_CAT_VID_NAME = "name";
    public final static String FB_CAT_VID_TAGS = "tags";
    public final static String FB_CAT_VID_THUMB_URL = "thumb_url";
    public final static String FB_CAT_VID_VIDEO_URL = "video_url";
    // championship
    public final static String FB_CHAMP_LOGO_URL = "logo_url";
    public final static String FB_CHAMP_NAME = "name";
    // channel
    public final static String FB_CHANNEL_DESC = "desc";
    public final static String FB_CHANNEL_LOGO = "logo";
    public final static String FB_CHANNEL_NAME = "name";
    // channel media
    public final static String FB_CH_MEDIA_BODY = "body";
    public final static String FB_CH_MEDIA_IS_DEFAULT = "is_default";
    public final static String FB_CH_MEDIA_NAME = "name";
    public final static String FB_CH_MEDIA_TYPE = "type";
    // club
    public final static String FB_CLUB_COUNTRY = "country";
    public final static String FB_CLUB_CREATION_DATE = "creation_date";
    public final static String FB_CLUB_LOGO_URL = "logo_url";
    public final static String FB_CLUB_NAME = "name";
    // commentator
    public final static String FB_COMM_COUNTRY = "country";
    public final static String FB_COMM_NAME = "name";
    public final static String FB_COMM_PIC_URL = "pic_url";
    // country
    public final static String FB_CNTRY_FLAG_URL = "flag_url";
    public final static String FB_CNTRY_NAME = "name";
    public final static String FB_CNTRY_TIMEZONE = "time_zone";
    // match
    public final static String FB_MATCH_ID = "id";
    public final static String FB_MATCH_CHAMP_ID = "champ_id";
    public final static String FB_MATCH_CHAMP_LOGO_URL = "champ_logo_url";
    public final static String FB_MATCH_CHAMP_NAME = "champ_name";
    public final static String FB_MATCH_CHANNEL_ID = "channel_id";
    public final static String FB_MATCH_CHANNEL_NAME = "channel_name";
    public final static String FB_MATCH_CLUB1_ID = "club1_id";
    public final static String FB_MATCH_CLUB1_LOGO_URL = "club1_logo_url";
    public final static String FB_MATCH_CLUB1_NAME = "club1_name";
    public final static String FB_MATCH_CLUB1_SCORE = "club1_score";
    public final static String FB_MATCH_CLUB2_ID = "club2_id";
    public final static String FB_MATCH_CLUB2_LOGO_URL = "club2_logo_url";
    public final static String FB_MATCH_CLUB2_NAME = "club2_name";
    public final static String FB_MATCH_CLUB2_SCORE = "club2_score";
    public final static String FB_MATCH_COMMENTATOR_ID = "commentrator_id";
    public final static String FB_MATCH_COMMENTATOR_NAME = "commentrator_name";
    public final static String FB_MATCH_DESC = "desc";
    //public final static String FB_MATCH_ID = "match_id";
    public final static String FB_MATCH_STATUS = "match_status";
    public final static String FB_MATCH_REFEREE_ID = "referee_id";
    public final static String FB_MATCH_REFEREE_NAME = "referee_name";
    public final static String FB_MATCH_STADIUM_ID = "staduim_id";
    public final static String FB_MATCH_STADIUM_NAME = "staduim_name";
    public final static String FB_MATCH_TAGS = "tags";
    public final static String FB_MATCH_TIME = "time";
    // news
    public final static String FB_NEWS_CREATION_DATE = "creation_date";
    public final static String FB_NEWS_DETAILS = "details";
    public final static String FB_NEWS_IS_IMPORTANT = "is_important";
    public final static String FB_NEWS_PHOTO_URL = "photo_url";
    public final static String FB_NEWS_TAGS = "tags";
    public final static String FB_NEWS_TITLE = "title";
    public final static String FB_NEWS_CATEGORY_ID = "news_category_id";
    public final static String FB_NEWS_CATEGORY_NAME = "news_category_name";
    // news category
    public final static String FB_NEWS_CAT_NAME = "name";
    // referee
    public final static String FB_REF_COUNTRY = "country";
    public final static String FB_REF_LEVEL = "level";
    public final static String FB_REF_NAME = "name";
    public final static String FB_REF_PIC_URL = "pic_url";
    // stadium
    public final static String FB_STAD_CITY = "city";
    public final static String FB_STAD_NAME = "name";
    public final static String FB_STAD_SEATS = "seats_number";
    // video
    public final static String FB_VID_CREATION_DATE = "creation_date";
    public final static String FB_VID_DESCRIPTION = "description";
    public final static String FB_VID_IS_IMPORTANT = "is_important";
    public final static String FB_VID_NAME = "name";
    public final static String FB_VID_TAGS = "tags";
    public final static String FB_VID_THUMB_URL = "thumb_url";
    public final static String FB_VID_VIDEO_URL = "video_url";
    public final static String FB_VID_VIDEO_REF = "video_ref";
    public final static String FB_VID_VIDEO_FRAME = "video_iframe";
    public final static String FB_VID_CAT_ID = "video_category_id";
    public final static String FB_VID_CAT_NAME = "video_category_name";
    // video category
    public final static String FB_VID_CATEGORY_NAME = "name";

  /**/

    public final static int YESTERDAY = 0;
    public final static int TODAY = 1;
    public final static int TOMORROW = 2;

    // Settings

    public static final String KEY_TIMEZONE = "timezone_settings";
    public static final String KEY_COUNTRY_INDEX = "country_index";
    public final static String KEY_MATCH_NOTIFICATION_ENABLE = "match_notification";
    public final static String KEY_NEWS_NOTIFICATION_ENABLE = "news_notification";
    public final static String KEY_GOAL_NOTIFICATION_ENABLE = "goal_notification";
    public final static String KEY_VIDEO_NOTIFICATION_ENABLE = "video_notification";
    public final static String KEY_MATCH_NOTIFICATION_SOUND = "match_notification_sound";
    public final static String KEY_NEWS_NOTIFICATION_SOUND = "news_notification_sound";
    public final static String KEY_GOAL_NOTIFICATION_SOUND = "goal_notification_sound";
    public final static String KEY_VIDEO_NOTIFICATION_SOUND = "video_notification_sound";

    public final static int FLAG_NOTIFICATION_SOUND = 0;
    public final static int FLAG_NOTIFICATION_MUTE = 1;
    public final static int FLAG_NOTIFICATION_VIBRATE = 2;

    public static final String CURRENT_POSITION = "current_video_position";
    public static final int FULL_SCREEN_CODE = 403;
    public static final String IS_PLAYING = "is_video_playing";

    public static final String FB_ADS_BANNER = "banner_ad_id";
    public static final String FB_ADS_INTERSTITIAL = "interstitial_ad_id";
    public static final String FB_ADS_OTHER = "other";
    public static final int MEDIA_TYPE_URL = 0;
    public static final int MEDIA_TYPE_FRAME = 1;
    public static final String ARABIC_LOCALE = "ar";

    public final static String KEY_NOTIFICATION_ID = "id";
    public final static String KEY_NOTIFICATION_TYPE = "type";
    public final static String KEY_NOTIFICATION_DATA = "object";
}
