package com.brighteyes.mykora.entities;

import com.brighteyes.mykora.helper.CommonConstants;
import com.google.firebase.database.DataSnapshot;

import org.parceler.Parcel;

/**
 * Created by Shanan on 18/07/2017.
 */

@Parcel
public class GoogleAdsId {
    String id;
    String bannerAdId;
    String interstitialAdId;
    String otherAdId;

    public GoogleAdsId() {
    }

    public GoogleAdsId parseAdsId(DataSnapshot dataSnapshot) {
        this.id = dataSnapshot.getKey();
        this.bannerAdId = (String) dataSnapshot.child(CommonConstants.FB_ADS_BANNER).getValue();
        this.interstitialAdId = (String) dataSnapshot.child(CommonConstants.FB_ADS_INTERSTITIAL).getValue();
        this.otherAdId = (String) dataSnapshot.child(CommonConstants.FB_ADS_OTHER).getValue();
        return this;
    }

    public String getId() {
        return id;
    }

    public String getBannerAdId() {
        return bannerAdId;
    }

    public String getInterstitialAdId() {
        return interstitialAdId;
    }

    public String getOtherAdId() {
        return otherAdId;
    }

}
