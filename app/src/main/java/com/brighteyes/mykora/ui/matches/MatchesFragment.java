package com.brighteyes.mykora.ui.matches;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brighteyes.mykora.R;
import com.brighteyes.mykora.ui.base.BaseSupportFragment;
import com.brighteyes.mykora.ui.base.PagerAdapter;
import com.brighteyes.mykora.ui.custom.CustomTabLayout;
import com.brighteyes.mykora.ui.custom.NonSwipeableViewPager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MatchesFragment extends BaseSupportFragment {

    @BindView(R.id.matches_viewpager)
    NonSwipeableViewPager mViewPager;
    @BindView(R.id.content_tabs)
    CustomTabLayout tabLayout;

    private PagerAdapter mPagerAdapter;
    private List<BaseSupportFragment> fragments = new ArrayList<>();

    public MatchesFragment() {
        // Required empty public constructor
    }

    public static MatchesFragment newInstance(String title) {
        MatchesFragment fragment = new MatchesFragment();
        fragment.setTitle(title);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_matches, container, false);
        ButterKnife.bind(this, rootView);

        initTabs();

        return rootView;
    }

    private void initTabs() {
        List<String> tabsTitles = Arrays.asList(getResources().getStringArray(R.array.matchesTabsTitles));
        if (fragments.size() == 0) {
            fragments.add(YesterdayFragment.newInstance(tabsTitles.get(0)));
            fragments.add(TodayFragment.newInstance(tabsTitles.get(1)));
            fragments.add(TomorrowFragment.newInstance(tabsTitles.get(2)));
        }
        mPagerAdapter = new PagerAdapter(getChildFragmentManager(), fragments);

        addDividers(tabLayout);
        mViewPager.setAdapter(mPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);
        mViewPager.setCurrentItem(1);
    }
}