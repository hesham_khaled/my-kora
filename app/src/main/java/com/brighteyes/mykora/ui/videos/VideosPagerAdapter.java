package com.brighteyes.mykora.ui.videos;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.brighteyes.mykora.ui.base.BaseSupportFragment;

import java.util.List;

/**
 * Created by Shanan on 20/02/2017.
 */

public class VideosPagerAdapter extends FragmentPagerAdapter {

    private final boolean mIsRtl;
    private List<BaseSupportFragment> fragments;

    public VideosPagerAdapter(FragmentManager fm, List<BaseSupportFragment> fragments, boolean isRtl) {
        super(fm);
        this.fragments = fragments;
        mIsRtl = isRtl;
    }

    @Override
    public Fragment getItem(int position) {
        if (mIsRtl && fragments != null && fragments.size() > 0) {
            return fragments.get(fragments.size() - 1);
        } else {
            return fragments.get(position);
        }
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (mIsRtl && fragments != null && fragments.size() > 0) {
            return fragments.get(fragments.size() - 1).getTitle();
        } else {
            return fragments.get(position).getTitle();
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        try {
            super.finishUpdate(container);
        } catch (Exception nullPointerException) {
            System.out.println("Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
        }
    }

}